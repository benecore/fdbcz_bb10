/*
 * ActiveFrame.cpp
 *
 *  Created on: 16-10-2012
 *      Author: Igor Andruszkiewicz
 */

#include "ActiveFrame.h"
#include <QDebug>

ActiveFrame::ActiveFrame() :
SceneCover(this),
_baseModel(0),
_favoriteModel(0)
{
	create();
}


ActiveFrame::ActiveFrame(bb::cascades::GroupDataModel *base, bb::cascades::GroupDataModel *favorite) :
		SceneCover(this),
		_baseModel(base),
		_favoriteModel(favorite)
{
	create();
}


ActiveFrame::~ActiveFrame() {
	// TODO Auto-generated destructor stub
}

void ActiveFrame::onThumbnail()
{
	current = 0;
	qDebug() << "START ACTIVE FRAME";
	timer->start(1000);
}

void ActiveFrame::onAwake()
{
	current = 0;
	qDebug() << "STOP ACTIVE FRAME";
	timer->stop();
}

void ActiveFrame::create()
{
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(update()));
	connect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(onThumbnail()));
	connect(Application::instance(), SIGNAL(awake()), this, SLOT(onAwake()));
	/**
	 * Remember to set permission Run When Backgrounded in bar-descriptor.xml.
	 */
	qml = QmlDocument::create("asset:///ActiveFrame.qml").parent(this);
	mMainContainer = qml->createRootObject<Container>();

	// Set the content of ActiveFrame
	setContent(mMainContainer);

	// You can do it now or trigger the slot whne application is moved to baackground.
	// QObject::connect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(update()));
	// and/or disable updates when it's in foreground
}

void ActiveFrame::update() {

	qDebug() << " UPDATING COVER";
	/**
	 *  You can update the cover here.
	 */

	//You can update AF every 1 sec but BB10 platform will update UI every 30sec :)

	Label* label2 = mMainContainer->findChild<Label*>("label2");

	if (label2 && _favoriteModel->size()) {
		qDebug() << "OK";
		//QVariant program = _favoriteModel->data(QVariantList() << 0 << current);
		//qDebug() << program.toMap().value("name").toString();
		//label2->setText("trla");
	}
	current++;
}

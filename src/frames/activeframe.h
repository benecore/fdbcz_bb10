/*
 * ActiveFrame.h
 *
 *  Created on: 16-10-2012
 *      Author: Igor Andruszkiewicz
 */

#ifndef ACTIVEFRAME_H_
#define ACTIVEFRAME_H_

#include <bb/cascades/SceneCover>
#include <bb/cascades/Container>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Label>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ImageView>

#include "../custom/webimageview.h"

using namespace ::bb::cascades;

class ActiveFrame: public SceneCover {
	Q_OBJECT
public:
	ActiveFrame();
	ActiveFrame(bb::cascades::GroupDataModel *base, bb::cascades::GroupDataModel *favorite);
	virtual ~ActiveFrame();

public slots:
	void update();


private slots:
    void onThumbnail();
    void onAwake();

protected:
	void create();

private:
	Q_DISABLE_COPY(ActiveFrame)
	Container *mMainContainer;
	QmlDocument *qml;
	GroupDataModel *_baseModel;
	GroupDataModel *_favoriteModel;
	QTimer *timer;
	int current;
};

#endif /* ACTIVEFRAME_H_ */

#include "WebImageView.h"
#include <QNetworkReply>
#include <QNetworkDiskCache>
#include <QtGui/QDesktopServices>
#include <bb/cascades/Image>

#include <QThread>

QNetworkAccessManager * WebImageView::mNetManager = new QNetworkAccessManager();
QNetworkDiskCache * WebImageView::mNetworkDiskCache = new QNetworkDiskCache();

WebImageView::WebImageView() :
																						_cache(false)
{
	// Initialize network cache
	mNetworkDiskCache->setCacheDirectory(QDesktopServices::storageLocation(QDesktopServices::CacheLocation));

	// Set cache in manager
	mNetManager->setCache(mNetworkDiskCache);

	// Set defaults
	mLoading = 0;
}

const QUrl& WebImageView::url() const {
	return mUrl;
}


const QUrl& WebImageView::defaultImage() const {
	return _defaultImage;
}



void WebImageView::setDefaultImage(const QUrl &url)
{
	_defaultImage = url;
	setImage(Image(_defaultImage));
}


void WebImageView::imageDataFinished(QByteArray data)
{
	if (data.isEmpty()){
		getRemote();
	}else{
		setImage(Image(data));
	}
}


void WebImageView::setUrl(const QUrl& url) {

	if (url.toString().contains("undefined")){
		setImage(Image(_defaultImage));
		return;
	}
	if (url.toString().contains("asset:///")){
		qDebug() << "ASSETS FILE";
		setImage(Image(url));
		return;
	}

	// Variables
	mUrl = url;
	mLoading = 0;

	// Reset the image
	resetImage();

	thumbFile = getHash(mUrl);
	if (_cache && QFile::exists(QString("data/thumbnails/%1").arg(thumbFile))){
		/*ImageProcessor *imageProcessor = new ImageProcessor(thumbFile);

		QFuture<QImage> future = QtConcurrent::run(imageProcessor, &ImageProcessor::start);

		// Invoke our onProcessingFinished slot after the processing has finished.
		bool ok = connect(&m_watcher, SIGNAL(finished()), this, SLOT(imageProcessingEnded()));
		Q_ASSERT(ok);
		Q_UNUSED(ok);

		// starts watching the given future
		m_watcher.setFuture(future);
*/

		QThread *thread = new QThread;
		ImageThread *imageThread = new ImageThread(thumbFile);
		imageThread->moveToThread(thread);
		connect(imageThread, SIGNAL(imageData(QByteArray)), this, SLOT(imageDataFinished(QByteArray)), Qt::QueuedConnection);
		connect(thread, SIGNAL(started()), imageThread, SLOT(process()), Qt::QueuedConnection);
		connect(imageThread, SIGNAL(finished()), thread, SLOT(quit()), Qt::QueuedConnection);
		connect(imageThread, SIGNAL(finished()), imageThread, SLOT(deleteLater()), Qt::QueuedConnection);
		connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()), Qt::QueuedConnection);
		thread->start();

	}else{
		getRemote();
	}

	emit urlChanged();
}

double WebImageView::loading() const {
	return mLoading;
}

void WebImageView::imageLoaded() {
	// Get reply
	QNetworkReply * reply = qobject_cast<QNetworkReply*>(sender());

	if (reply->error() == QNetworkReply::NoError) {
		if (isARedirectedUrl(reply)) {
			setURLToRedirectedUrl(reply);
			return;
		} else {
			thumbFile = getHash(mUrl);
			imageData = reply->readAll();
			if (_cache && !QFile::exists(QString("data/thumbnails/%1").arg(thumbFile)) && !Helper::instance()->cacheReached()){
				/*ImageProcessor *imageProcessor = new ImageProcessor(thumbFile, imageData);

				QFuture<QImage> future = QtConcurrent::run(imageProcessor, &ImageProcessor::start);

				// Invoke our onProcessingFinished slot after the processing has finished.
				bool ok = connect(&m_watcher, SIGNAL(finished()), this, SLOT(imageProcessingEnded()));
				Q_ASSERT(ok);
				Q_UNUSED(ok);

				// starts watching the given future
				m_watcher.setFuture(future);
*/

				QThread *thread = new QThread;
				ImageThread *imageThread = new ImageThread(thumbFile, imageData);
				imageThread->moveToThread(thread);
				connect(thread, SIGNAL(started()), imageThread, SLOT(process()), Qt::QueuedConnection);
				connect(imageThread, SIGNAL(finished()), thread, SLOT(quit()), Qt::QueuedConnection);
				connect(imageThread, SIGNAL(finished()), imageThread, SLOT(deleteLater()), Qt::QueuedConnection);
				connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()), Qt::QueuedConnection);
				thread->start();
			}
			setImage(Image(imageData));
		}
	}else{
		setImage(Image(_defaultImage));
	}

	// Memory management
	reply->deleteLater();
}

bool WebImageView::isARedirectedUrl(QNetworkReply *reply) {
	QUrl redirection = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
	return !redirection.isEmpty();
}

void WebImageView::getRemote()
{
	// Create request
	QNetworkRequest request;
	request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
	request.setUrl(mUrl);

	// Create reply
	QNetworkReply * reply = mNetManager->get(request);

	// Connect to signals
	QObject::connect(reply, SIGNAL(finished()), this, SLOT(imageLoaded()));
	QObject::connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(dowloadProgressed(qint64,qint64)));
}

void WebImageView::setURLToRedirectedUrl(QNetworkReply *reply) {
	QUrl redirectionUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
	QUrl baseUrl = reply->url();
	QUrl resolvedUrl = baseUrl.resolved(redirectionUrl);

	setUrl(resolvedUrl.toString());
}

void WebImageView::clearCache() {
	mNetworkDiskCache->clear();
}

void WebImageView::dowloadProgressed(qint64 bytes, qint64 total) {
	mLoading = double(bytes) / double(total);

	emit loadingChanged();
}

void WebImageView::imageProcessingEnded()
{
	resetImage();
	QImage swappedImage = m_watcher.result().rgbSwapped();
	if(swappedImage.format() != QImage::Format_RGB32) {
		swappedImage = swappedImage.convertToFormat(QImage::Format_RGB32);
	}
	const bb::ImageData imageDataTemp = bb::ImageData::fromPixels(swappedImage.bits(), bb::PixelFormat::RGBX, swappedImage.width(), swappedImage.height(), swappedImage.bytesPerLine());



	if (QFile::exists(QString("data/thumbnails/%1").arg(thumbFile))){
		qDebug() << "GETTING IMAGE DONE";
		setImage(Image(imageDataTemp));
	}else{
		qDebug() << "SAVING IMAGE DONE";
		setImage(Image(imageDataTemp));
		imageData.clear();
	}
}

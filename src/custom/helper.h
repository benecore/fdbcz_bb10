#ifndef HELPER_H
#define HELPER_H

#include <QObject>
#include <QDir>
#include <QDateTime>
#include <QTime>
#include <bb/PpsObject>
#include <QFileInfo>
#include <QDebug>
#include <bb/cascades/Application>
#include <bb/cascades/CustomControl>
#include "../storage/settings.h"
#include "../storage/database.h"
#include "invoker.h"

using namespace bb::cascades;

class Helper : public bb::cascades::CustomControl
{
    Q_OBJECT
    Q_PROPERTY(QString thumbnailSizeString READ thumbnailSizeString NOTIFY thumbnailsChanged)
    Q_PROPERTY(qint64 thumbnailSize READ getThumbnailSize NOTIFY thumbnailsChanged)
    Q_PROPERTY(int thumbnailCount READ thumbnailCount NOTIFY thumbnailsChanged)
    Q_PROPERTY(bool cacheReached READ cacheReached NOTIFY cacheChanged)
public:
    explicit Helper();
    virtual ~Helper();


    static Helper *instance();
    static void destroy();

    Q_INVOKABLE
    QString getMonthName(const int &month = 0);
    Q_INVOKABLE
    bool saveFile(const QString &path);
    Q_INVOKABLE
    bool restoreFile(const QString &path);
    Q_INVOKABLE
    int getTimestamp(const QString &dateTime, const QString &inputFormat = "yyyy-MM-dd HH:mm");
    Q_INVOKABLE
    inline int getCurrentTimestamp() const { return QDateTime::currentDateTime().toTime_t(); }
    Q_INVOKABLE
    QString increaseTime(const QString &time, const int &seconds = 0, const QString &outputFormat = "yyyy-MM-dd HH:mm:ss", const QString &inputFormat = "yyyy-MM-dd HH:mm:ss");
    Q_INVOKABLE
    QString getDate(const int &day = 0, const QVariant &format = QVariant());
    Q_INVOKABLE
    int getDuration(const QString &startTime, const QString &endTime);
    Q_INVOKABLE
    QVariant increaseDate(const QVariant &date, const int &minute);
    Q_INVOKABLE
    QString getTime(const int &sec = 0);
    Q_INVOKABLE
    float getPercentage(QVariant start, QVariant end);
    Q_INVOKABLE
    inline QString thumbnailSizeString() { return convSize(getThumbnailSize()); }
    Q_INVOKABLE
    qint64 getThumbnailSize();
    Q_INVOKABLE
    int thumbnailCount();
    Q_INVOKABLE
    bool clearThumbnails(const QString & dirName = "data/thumbnails");
    Q_INVOKABLE
    inline QByteArray encodeObject(const QVariantMap &ppsData, bool *ok=0){
    	return bb::PpsObject::encode(ppsData, ok);
    }
    Q_INVOKABLE
    QString convSize(const qint64 &size);
    Q_INVOKABLE
    void openInBrowser(const QString &id);
    Q_INVOKABLE
    void invokeBrowser(const QString &url);

	inline bool cacheReached() {
		long int limit = Settings::instance()->cacheLimit();
		long int current =  getThumbnailSize();
		//qDebug() << "LIMIT:" << limit << endl << "CURRENT:" << current;
		//qDebug() << "BOOL: " << (current > limit);
		return (current >= limit);
	}



signals:
    void thumbnailsChanged();
    void cacheChanged();

private:
    Q_DISABLE_COPY(Helper)
	static Helper *_instance;
    Invoker *invoker;
};

#endif // HELPER_H

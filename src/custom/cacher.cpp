/*
 * cacher.cpp
 *
 *  Created on: 6.3.2014
 *      Author: benecore
 */

#include "cacher.h"
#include <QDebug>

const char* const CACHE_FILE = "data/cache.json";

Cacher *Cacher::_instance = 0;

Cacher::Cacher()
{
	_cache = new QFile(CACHE_FILE);
}

Cacher::~Cacher()
{
	delete _cache;
}


Cacher *Cacher::instance()
{
	if (!_instance)
		_instance = new Cacher;
	return _instance;
}


void Cacher::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}

void Cacher::storeCache(const QByteArray& data)
{
	qDebug() << Q_FUNC_INFO;
	if (!_cache->open(QIODevice::WriteOnly)){
		qWarning() << "Unable write to the cache file" << _cache->errorString();
		return;
	}
	_cache->write(data);
	_cache->close();
}

QByteArray Cacher::getCache()
{
	if (!_cache->open(QIODevice::ReadOnly)){
		qWarning() << "Unable to get cache" << _cache->errorString();
		return QByteArray();
	}
	QByteArray data = _cache->readAll();
	_cache->close();
	return data;
}

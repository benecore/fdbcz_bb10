#ifndef WEBIMAGEVIEW_H_
#define WEBIMAGEVIEW_H_

#include <bb/cascades/ImageView>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QUrl>
#include <QCryptoGraphicHash>
#include <QFile>
#include <QDir>

#include "imagethread.h"
#include "imageprocessor.hpp"

using namespace bb::cascades;

class WebImageView: public bb::cascades::ImageView {
	Q_OBJECT
	Q_PROPERTY (QUrl url READ url WRITE setUrl NOTIFY urlChanged)
	Q_PROPERTY (float loading READ loading NOTIFY loadingChanged)
	Q_PROPERTY(QUrl defaultImage READ defaultImage WRITE setDefaultImage NOTIFY defaultImageChanged)
	Q_PROPERTY(bool cache READ cache WRITE setCache NOTIFY cacheChanged)
public:
	WebImageView();
	const QUrl& url() const;
	const QUrl &defaultImage() const;
	double loading() const;
	inline bool cache() { return _cache; }

public Q_SLOTS:
	void setUrl(const QUrl& url);
	void setDefaultImage(const QUrl &url);
	void clearCache();
	inline void setCache(const bool &value){
		if (_cache != value){
			_cache = value;
			emit cacheChanged();
		}
	}

private Q_SLOTS:
	void imageLoaded();
	void dowloadProgressed(qint64,qint64);
	void imageDataFinished(QByteArray data);


	void getRemote();

	inline QByteArray getHash(const QUrl &url){
		//return QCryptographicHash::hash(url.toString().toUtf8(), QCryptographicHash::Md5).toHex() + ".png";
		return url.toString().split("/").last().toUtf8();
	}

	void imageProcessingEnded();
/*
 *
	inline void saveImage(const QByteArray &data){
		QString thumbFile = getHash(mUrl);
		QFile image(QString("data/thumbnails/%1").arg(thumbFile));
		if (!image.open(QIODevice::WriteOnly)){
			qWarning() << Q_FUNC_INFO << "Unable to open file" << image.errorString();
		}
		image.write(data);
		image.close();
	}
	inline QByteArray getImage(){
		QString thumbFile = getHash(mUrl);
		QFile image(QString("data/thumbnails/%1").arg(thumbFile));
		if (!image.open(QIODevice::ReadOnly)){
			return QByteArray();
		}
		QByteArray data = image.readAll();
		image.close();
		return data;
	}*/


	signals:
	void urlChanged();
	void loadingChanged();
	void defaultImageChanged();
	void cacheChanged();


	private:
	static QNetworkAccessManager * mNetManager;
	static QNetworkDiskCache * mNetworkDiskCache;
	QUrl mUrl,
	_defaultImage;
	float mLoading;
	bool _cache;
	QFutureWatcher<QImage> m_watcher;
	QString thumbFile;
	QByteArray imageData;

	bool isARedirectedUrl(QNetworkReply *reply);
	void setURLToRedirectedUrl(QNetworkReply *reply);
};

#endif /* WEBIMAGEVIEW_H_ */

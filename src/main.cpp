/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <bb/cascades/Application>
#include <bb/ApplicationInfo>
#include <Flurry.h>
#include <QLocale>
#include <QTranslator>
#include "fdbcz.hpp"

#include <Qt/qdeclarativedebug.h>

using namespace bb::cascades;


QByteArray getTheme(){
	return ThemeSettings::instance()->getThemeString("theme").toUtf8();
}

Q_DECL_EXPORT int main(int argc, char **argv)
{

#if !defined(QT_NO_DEBUG)
	Flurry::Analytics::SetDebugLogEnabled(true);
#endif

	// Read and log the version of the app being used
	Flurry::Analytics::SetAppVersion(bb::ApplicationInfo().version());

	Flurry::Analytics::StartSession("47R64JC6KJYBRFFB4XBD");


	qputenv("CASCADES_THEME", getTheme());
    Application app(argc, argv);

    // Create the Application UI object, this is where the main.qml file
    // is loaded and the application scene is set.
    new FDBcz(&app);

    // Enter the application main event loop.
    return Application::exec();
}

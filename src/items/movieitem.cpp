#include "movieitem.h"
#include <QDebug>

MovieItem::MovieItem()
{
}


MovieItem::MovieItem(QVariantMap &item) :
    _item(item)
{
    emit dataChanged();
}



QVariantList MovieItem::movie_peoples()
{
    QVariantList list;
    QVariantMap item;

    foreach(const QVariant &director, _item.value("director").toList()){
        item = director.toMap();
        item.insert("type", tr("Director"));
        list.append(item);
    }
    foreach(const QVariant &scriptWriter, _item.value("scriptwriter").toList()){
        item = scriptWriter.toMap();
        item.insert("type", tr("Scriptwriter"));
        list.append(item);
    }
    foreach(const QVariant &storie, _item.value("storie").toList()){
        item = storie.toMap();
        item.insert("type", tr("Storie"));
        list.append(item);
    }
    foreach(const QVariant &music, _item.value("music").toList()){
        item = music.toMap();
        item.insert("type", tr("Music"));
        list.append(item);
    }
    foreach(const QVariant &camera, _item.value("camera").toList()){
        item = camera.toMap();
        item.insert("type", tr("Camera"));
        list.append(item);
    }
    foreach(const QVariant &editing, _item.value("editing").toList()){
        item = editing.toMap();
        item.insert("type", tr("Editing"));
        list.append(item);
    }
    foreach(const QVariant &actor, _item.value("actor").toList()){
        item = actor.toMap();
        item.insert("type", tr("Actor"));
        list.append(item);
    }
    foreach(const QVariant &performer, _item.value("performer").toList()){
        item = performer.toMap();
        item.insert("type", tr("Performer"));
        list.append(item);
    }
    foreach(const QVariant &dabing, _item.value("dabing").toList()){
        item = dabing.toMap();
        item.insert("type", tr("Dubbing"));
        list.append(item);
    }
    foreach(const QVariant &voice, _item.value("voice").toList()){
        item = voice.toMap();
        item.insert("type", tr("Voice"));
        list.append(item);
    }
    return list;
}

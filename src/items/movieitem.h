#ifndef MOVIEITEM_H
#define MOVIEITEM_H

#include <QObject>
#include <QVariantMap>
#include <QStringList>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

/*

  KINO OBJECT
        {
            "id": "5680",
            "name": "Multikino",
            "city": "Zlín",
            "web": "http://www.gacinema.cz/?go=multikino",
            "adress": "náměstí Míru 174, Zlín",
            "kraj": "Zlínský"
        }
  --------------------------
  VIDEO OBJECT

       {
            "thumbnail": "http://img.fdb.cz/trailery_m/9/img19198.jpg",
            "stream": "http://img.fdb.cz/trailery_m/9/936c1b942410fa1ddb2471ecb05ff.mp4"
       }
  --------------------------
  EPIZODA OBJECT
        {
            "id": "84306",
            "serie": "1",
            "dil": "1",
            "title": "Rozchod",
            "orig_title": "Pilot",
            "image": "edcf265cdc2927f1527cb1850c9.jpg",
            "year": "2003",
            "rating": "633"
        }
  */
class MovieItem : public bb::cascades::CustomControl
{
    Q_OBJECT
    Q_PROPERTY(QVariant id READ id NOTIFY dataChanged)
    Q_PROPERTY(QVariant url READ url NOTIFY dataChanged)
    Q_PROPERTY(QVariant image READ image NOTIFY dataChanged)
    Q_PROPERTY(QVariant title READ title NOTIFY dataChanged)
    Q_PROPERTY(QVariant orig_title READ orig_title NOTIFY dataChanged)
    Q_PROPERTY(QVariant next_title READ next_title NOTIFY dataChanged)
    Q_PROPERTY(QVariant year READ year NOTIFY dataChanged)
    Q_PROPERTY(QVariant length READ length NOTIFY dataChanged)
    Q_PROPERTY(QVariant motto READ motto NOTIFY dataChanged)
    Q_PROPERTY(QVariant rating READ rating NOTIFY dataChanged)
    Q_PROPERTY(QVariant rating_count READ rating_count NOTIFY dataChanged)
    Q_PROPERTY(QVariantList kina READ kina NOTIFY dataChanged)
    Q_PROPERTY(int pocetkin READ pocetkin NOTIFY dataChanged)
    Q_PROPERTY(int fotogalerie READ fotogalerie NOTIFY dataChanged)
    Q_PROPERTY(QVariantList video READ video NOTIFY dataChanged)
    Q_PROPERTY(QVariant info READ info NOTIFY dataChanged)
    Q_PROPERTY(int pocetdilu READ pocetdilu NOTIFY dataChanged)
    Q_PROPERTY(QVariantList epizody READ epizody NOTIFY dataChanged)
    Q_PROPERTY(QVariantList movie_peoples READ movie_peoples NOTIFY dataChanged)
    Q_PROPERTY(QVariantList comments READ comments NOTIFY dataChanged)
    Q_PROPERTY(QVariantList tv READ tv NOTIFY dataChanged)
    Q_PROPERTY(int pocettv READ pocettv NOTIFY dataChanged)
    Q_PROPERTY(QVariant zanry READ zanry NOTIFY dataChanged)
    Q_PROPERTY(QVariant zeme READ zeme NOTIFY dataChanged)
public:
    MovieItem();
    MovieItem(QVariantMap &item);


    Q_INVOKABLE inline QVariant id() const { return _item.value("id", "-1"); }
    Q_INVOKABLE inline QVariant url() const { return _item.value("url"); }
    Q_INVOKABLE inline QVariant image() const { return _item.value("image"); }
    Q_INVOKABLE inline QVariant title() const { return _item.value("title"); }
    Q_INVOKABLE inline QVariant orig_title() const { return _item.value("orig_title"); }
    Q_INVOKABLE inline QVariant next_title() const { return _item.value("next_title"); }
    Q_INVOKABLE inline QVariant year() const { return _item.value("year"); }
    Q_INVOKABLE inline QVariant length() const { return _item.value("length"); }
    Q_INVOKABLE inline QVariant motto() const { return _item.value("motto"); }
    Q_INVOKABLE inline QVariant rating() const { return _item.value("rating"); }
    Q_INVOKABLE inline QVariant rating_count() const { return _item.value("rating_count"); }
    Q_INVOKABLE inline QVariantList kina() const { return _item.value("kina").toList(); }
    Q_INVOKABLE inline int pocetkin() const { return _item.value("pocetkin").toInt(); }
    Q_INVOKABLE inline int fotogalerie() const { return _item.value("fotogalerie").toInt(); }
    Q_INVOKABLE inline QVariantList video() const { return _item.value("video").toList(); }
    Q_INVOKABLE inline QVariant info() const { return _item.value("info"); }
    Q_INVOKABLE inline int pocetdilu() const { return _item.value("pocetdilu").toInt(); } // Ak ide o serial tak pocet dielov
    Q_INVOKABLE inline QVariantList epizody() const { return _item.value("epizoda").toList(); }
    Q_INVOKABLE QVariantList movie_peoples();
    Q_INVOKABLE QVariantList comments() const { return _item.value("comment").toList(); }
    Q_INVOKABLE inline QVariantList tv() const { return _item.value("tv").toList(); }
    Q_INVOKABLE inline int pocettv() const { return _item.value("pocettv").toInt(); }
    Q_INVOKABLE inline QVariant zanry() const { return _item.value("zanry"); }
    Q_INVOKABLE inline QVariant zeme() const { return _item.value("zeme"); }



signals:
    void dataChanged();


private:
    QVariantMap _item;
};

#endif // MOVIEITEM_H

/*



*/

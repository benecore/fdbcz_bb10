/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FDBCZ_HPP_
#define FDBCZ_HPP_

#include <QObject>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/ThemeSupport>
#include <bb/cascades/Theme>
#include <bb/cascades/ColorTheme>
#include <bb/data/JsonDataAccess>

/** Live coding **/
#include "livecoding/qmlbeam.h"

/** Custom classes **/
#include "custom/webimageview.h"
#include "custom/sizehelper.h"
#include "custom/timer.h"
#include "custom/themesettings.h"
#include "custom/cacher.h"
#include "custom/helper.h"
#include "custom/invoker.h"
#include "custom/calendarhandler.h"
// platform services
#include "platform/InviteToDownload.hpp"
#include "platform/RegistrationHandler.hpp"
/* Frames */
#include "frames/activeframe.h"
#include "frames/frame.h"
/* Models */
#include "models/models.h"
// Items
#include "items/actoritem.h"
#include "items/movieitem.h"
/** Storage **/
#include "storage/settings.h"
#include "storage/database.h"

/** FDB library **/
#include <FDBLib>



namespace bb
{
namespace cascades
{
class Application;
class LocaleHandler;

namespace maps {
class MapView;
}
}
namespace platform {
namespace geo {
class GeoLocation;
}
}
}



class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class FDBcz : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QVariantList indexPath READ indexPath WRITE setIndexPath NOTIFY indexPathChanged)
	Q_PROPERTY(QString genTime READ genTime WRITE setGenTime NOTIFY genTimeChanged)
	Q_PROPERTY(bool loading READ loading WRITE setLoading NOTIFY loadingChanged)
	Q_PROPERTY(bool whiteTheme READ whiteTheme CONSTANT)
public:
	FDBcz(bb::cascades::Application *app);
	virtual ~FDBcz();

	Q_INVOKABLE
	void addPinToMap(QObject* mapObject, const QString &title, const QString &subtitle);
	Q_INVOKABLE
	void updateMarkers(QObject* mapObject, QObject* containerObject) const;

	Q_INVOKABLE
	void loadPraveBezi();
	Q_INVOKABLE
	inline void blocking(const bool &value){
		this->blockSignals(value);
	}
	Q_INVOKABLE
	void addToFavorite(const QVariantList &selectedItems);
	Q_INVOKABLE
	void removeFromFavorite(const QVariantList &selectedItems);
	Q_INVOKABLE
	void changeLanguage(const QString &language = "cs"/* ISO 639 */);
	Q_INVOKABLE
	inline QByteArray md5(QByteArray data)
	{
		return QCryptographicHash::hash(data, QCryptographicHash::Md5).toHex();
	}


	signals:
	void clearModel();
	void loadingChanged(bool loading);
	void genTimeChanged(const QString genTime);
	void indexPathChanged(const QVariantList indexpath);
	// Signals
	void baseItemDone(const QVariantMap item);
	void favoriteItemDone(const QVariantMap item);
	void reloadModel();
	void vysielanieDone(const QByteArray response);
	void programDone(const QByteArray response);
	void tvTipyDone(const QByteArray response);
	void detailFilmuDone(MovieItem *response);
	void detailHercaDone(ActorItem *response);
	void galeriaDone(const QByteArray response);
	void hladatDone(const QByteArray response);
	void hladatProgramDone(const QByteArray response);
	void kinoPremieryDone(const QByteArray response);
	void kinoMestaDone(const QByteArray response);
	void kinoZoznamDone(const QByteArray response);
	void kinoProgramDone(const QByteArray response);
	void kinoProgramOblubeneDone(const QVariantMap item);
	void loginDone(const QByteArray response);
	void registerDone(const QByteArray response);
	void mojeChcemVidietDone(const QByteArray response);
	void mojeVidelSomDone(const QByteArray response);
	void chcemVidietDone(const QByteArray response);
	void videlSomDone(const QByteArray response);
	void mojFilmDone(const QByteArray response);
	void hodnotenieDone(const QByteArray response);
	void mojeHodnoteniaDone(const QByteArray response);
	void mojeKomentareDone(const QByteArray response);
	void mojeVidelHodnotilDone(const QByteArray response);


	private slots:
	QPoint worldToPixel(QObject* mapObject, double latitude, double longitude) const;
	void onSystemLanguageChanged();
	/** API signals **/
	void requestReady(const QByteArray networkReply, const QNetworkReply *reply);
	void error(int errorCode, QString errorString);
	/** Properties **/
	inline QVariantList indexPath() const { return _indexPath; };
	inline void setIndexPath(const QVariantList &value){
		if (_indexPath != value){
			_indexPath = value;
			emit indexPathChanged(_indexPath);
		}
	}
	inline bool loading() const { return _loading; };
	inline void setLoading(const bool &value){
		if (_loading != value){
			_loading = value;
			emit loadingChanged(_loading);
		}
	}
	inline QString genTime() const { return _genTime; };
	inline void setGenTime(const QString &value){
		if (_genTime != value){
			_genTime = value;
			emit genTimeChanged(_genTime);
		}
	}
	inline bool whiteTheme() const { return Application::instance()->themeSupport()->theme()->colorTheme()->style() == VisualStyle::Bright; }
	void onThumbnail();
	void onAwake();


	// Parsing responses
	void parseBeziOd(const QByteArray &response);
	void parsePraveBezi(const QByteArray &response);
	void parseVysielanie(const QByteArray &response);
	void parseProgram(const QByteArray &response);
	void parseTvTipy(const QByteArray &response);
	void parseDetailFilmu(const QByteArray &response);
	void parseDetailHerca(const QByteArray &response);
	void parseGaleriaFilmu(const QByteArray &response);
	void parseHladatFilm(const QByteArray &response);
	void parseHladatHerca(const QByteArray &response);
	void parseHladatProgram(const QByteArray &response);
	void parseKinoPremiery(const QByteArray &response);
	void parseKinoMesta(const QByteArray &response);
	void parseKinoZoznam(const QByteArray &response);
	void parseKinoProgram(const QByteArray &response);
	void parseKinoProgramOblubene(const QByteArray &response);
	void parseRegister(const QByteArray &response);
	void parseLogin(const QByteArray &response);
	void parseMojeChcemVidiet(const QByteArray &response);
	void parseMojeVidelSom(const QByteArray &response);
	void parseChcemVidiet(const QByteArray &response);
	void parseVidelSom(const QByteArray &response);
	void parseMojFilm(const QByteArray &response);
	void parseHodnotenie(const QByteArray &response);
	void parseMojeHodnotenia(const QByteArray &response);
	void parseMojeKomentare(const QByteArray &response);
	void parseMojeVidelHodnotil(const QByteArray &response);


	private:
	QTranslator* m_pTranslator;
	bb::cascades::LocaleHandler* m_pLocaleHandler;
	FDBLib *api;
	Settings *settings;
	SizeHelper *sizeHelper;
	bool _loading;
	QString _genTime;
	QVariantList _indexPath;
	GroupDataModel *_baseModel;
	GroupDataModel *_favoriteModel;
	QTimer *_timer;
	RegistrationHandler *_registrationHandler;
	InviteToDownload *_inviteDownload;
};

#endif /* FDBCZ_HPP_ */

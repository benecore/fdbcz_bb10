/*
     (FDBLib_global.h)
     
    Copyright (c) 2014 Zoltán Benke (Benecore) benecore@devpda.net
                                               http://devpda.net
                                               
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
 */

#ifndef FDBLIB_EXPORT_H
#define FDBLIB_EXPORT_H

#include <QtCore/qglobal.h>

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#define QT5
#endif

#if defined(FDBLIB)
#  define FDBLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define FDBLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // FDBLIB_EXPORT_H

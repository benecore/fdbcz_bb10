import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    MyPage {
        id: root
        
        function request(response){
            var json = JSON.parse(response)
            for (var item in json.data){
                var kino = json.data[item]
                var datum = kino.date.split(".");
                var timestamp = new Date(Date.UTC(datum[2], datum[1]-1, datum[0])).getTime()/1000;
                kino.timestamp = timestamp
                model.insert(kino);
            }
            App.loading = false
        }
        
        header: TitleHeader {
            titleText: activeTab.title
            subtitleText: activeTab.description
        }
        loading: App.loading
        onActiveChanged: {
            if (active){
                App.kinoPremieryDone.connect(request)
                App.loading = true
                Api.kinoPremiery(2)
            }
        }
        
        
        actions: [
            RefreshAction {
                ActionBar.placement: ActionBarPlacement.OnBar
                onClicked: {
                    App.loading = true
                    model.clear()
                    Api.kinoPremiery(kinoPremierySelection.selectedValue)
                }
            }
        ]
        
        
        Container {
            SegmentedControl {
                id: kinoPremierySelection
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: Helper.getMonthName() + Retranslate.onLocaleOrLanguageChanged
                        value: 2
                    },
                    Option {
                        text: Helper.getMonthName(1) + Retranslate.onLocaleOrLanguageChanged
                        value: 3
                    }
                ]
                onSelectedOptionChanged: {
                    App.loading = true
                    model.clear() 
                    Api.kinoPremiery(selectedValue)
                }
                bottomMargin: 0
            }
            
            ListView {
                id: listView
                
                function addNotify(indexPath){
                    var item = model.data(indexPath)
                    // {"title": Nazov filmu, "body": Popis filmu, "station": Nazov stanice, "guid": Unikatne ID}
                    var data = {"title": item.film.title, "body": item.film.zanry, "station": item.name, "guid": item.idz}
                    Calendar.addEvent(item.alarm, item.min, data)
                }
                
                scrollRole: ScrollRole.Main
                opacity: App.loading ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                dataModel: GroupDataModel {
                    id: model
                    grouping: ItemGrouping.None
                    sortingKeys: ["timestamp"]
                    sortedAscending: true
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        ListItemKinoPremiery {}
                    }
                ]
                layout: StackListLayout {
                    headerMode: ListHeaderMode.Sticky
                }
                bufferedScrollingEnabled: true
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    if (item){
                        var page = detailFilmu.createObject()
                        page.title = item.film
                        page.filmId = item.id
                        activePane.push(page);
                    }
                }
                
                attachedObjects: [
                    ComponentDefinition {
                        id: detailFilmu
                        source: "../pages/DetailFilmu.qml"
                    }
                ]
            }
        }
    }
}

import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../items"
import "../actions"
import "../sheet"
import "../pages"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    MyPage {
        id: root
        
        function login(){
            if (usernameField.text.length && passwordField.text.length){
                App.loading = true
                console.log("PASSWORD MD5: "+App.md5(passwordField.text.trim()))
                Api.login(usernameField.text.trim(), passwordField.text.trim())
            }else{
                if (!usernameField.text.length){
                    toast.body = qsTr("You must fill email to login") + Retranslate.onLocaleOrLanguageChanged
                    usernameField.requestFocus()
                }else if (!passwordField.text.length){
                    toast.body = qsTr("You must fill password to login") + Retranslate.onLocaleOrLanguageChanged
                    passwordField.requestFocus()
                }
                toast.show()
            }
        }
        
        function register(username, email){
            Api.registrovat(username, email)
        }
        
        
        function signOut(){
            toast.button.label = ""
            rememberLogins.checked = false
            Database.clearLogin()
            Api.clearLogin();
            root.isLogged = false
        }
        
        function loginDone(response){
            var json = JSON.parse(response)
            if (json.error == "OK"){
                toast.body = qsTr("Login success") + Retranslate.onLocaleOrLanguageChanged
                if (rememberLogins.checked){
                    Database.setLogin(usernameField.text.trim(), App.md5(passwordField.text.trim()))
                }
                Api.setLoginDetails(usernameField.text.trim(), App.md5(passwordField.text.trim()))
                root.isLogged = true
                console.log("Login success")
            }else if (json.error == "Spatny email"){
                toast.body = qsTr("Login failed. Check your email adress") + Retranslate.onLocaleOrLanguageChanged
                console.log("Bad email adress")
            }else if (json.error == "Spatny userkey"){
                toast.body = qsTr("Login failed. Check your password") + Retranslate.onLocaleOrLanguageChanged
                console.log("Bad password")
            }
            App.loading = false
            toast.show()
        }
        
        function registerDone(response){
            var json = JSON.parse(response)
            if (json.error == "OK"){
                toast.body = qsTr("Registration success. Your password was generated automatically and sended to your email. You've been automatically logged in.") + Retranslate.onLocaleOrLanguageChanged
                toast.button.label = qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
                toast.show()
                Database.setLogin(json.email.trim(), json.password.trim())
                Api.setLoginDetails(json.email.trim(), json.password.trim())
            }else{
                toast.body = json.error
                toast.show()
            }
            
        }
        
        property bool isLogged: Database.isLogged || Api.isLogged()
        onIsLoggedChanged: {
            if (isLogged){
                root.addAction(actionComponent.createObject())
            }else{
                root.removeAllActions()
            }
        }
        header: TitleHeader {
            titleText: activeTab.title
        }
        loading: false
        
        
        onCreationCompleted: {
            App.registerDone.connect(registerDone)
            App.loginDone.connect(loginDone)
        }
        
        
        onActiveChanged: {
            if (active){
                if (!root.isLogged)
                	usernameField.requestFocus()
            }
        }
        
        attachedObjects: [
            SystemToast {
                id: toast
                autoUpdateEnabled: true
                onFinished: {
                    if (value == SystemUiResult.ButtonSelection){
                        toast.cancel()
                    }
                }
            },
            ComponentDefinition {
                id: registerSheet
                Register {
                    onRegisterClicked: {
                        root.register(username, email)
                    }
                }
            },
            ComponentDefinition {
                id: chcemVidiet
                source: "../pages/AccountChcemVidiet.qml"
            },
            ComponentDefinition {
                id: videlSom
                source: "../pages/AccountVidelSom.qml"
            },
            ComponentDefinition {
                id: hodnotilSom
                source: "../pages/AccountHodnotilSom.qml"
            },
            ComponentDefinition {
                id: actionComponent
                SignOutAction {
                    ActionBar.placement: ActionBarPlacement.OnBar
                    onClicked: {
                        root.signOut()
                    }
                }
            }
        ]
        
        
        ActivityIndicator {
            preferredHeight: 300
            preferredWidth: 300
            visible: App.loading
            running: visible
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
        }
        
     
        ScrollView {
            enabled: !App.loading
            scrollRole: ScrollRole.Main
            scrollViewProperties.pinchToZoomEnabled: false
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Container {
                leftPadding: 20
                rightPadding: 20
                topPadding: 20
                bottomPadding: 20
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                ImageView {
                    preferredHeight: 150
                    scalingMethod: ScalingMethod.AspectFit
                    leftMargin: 50
                    rightMargin: 50
                    topMargin: 50
                    bottomMargin: 50
                    imageSource: App.whiteTheme ? "asset:///images/fdbcz_white.png" : "asset:///images/fdbcz_black.png"
                    horizontalAlignment: HorizontalAlignment.Center
                }
                
                Container {
                    visible: !root.isLogged
                    TextField {
                        id: usernameField
                        hintText: "email"
                        inputMode: TextFieldInputMode.EmailAddress
                        input.submitKey: SubmitKey.Next
                        input.onSubmitted: {
                            passwordField.requestFocus()
                        }
                    }
                    
                    TextField {
                        id: passwordField
                        hintText: qsTr("password") + Retranslate.onLocaleOrLanguageChanged
                        inputMode: TextFieldInputMode.Password
                        input.submitKey: SubmitKey.EnterKey
                        input.onSubmitted: {
                            root.login()
                        }
                    }
                    
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        CheckBox {
                            id: rememberLogins
                            verticalAlignment: VerticalAlignment.Center
                        }
                        
                        Label {
                            opacity: !App.loading ? 1 : 0.5
                            text: qsTr("Remember login details") + Retranslate.onLocaleOrLanguageChanged
                        }
                    }
                    
                    Button {
                        preferredWidth: SizeHelper.maxWidth
                        horizontalAlignment: HorizontalAlignment.Center
                        text: qsTr("Sign In") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            root.login()
                        }
                    }
                    
                    Label {
                        topMargin: 100
                        textStyle{
                            fontSize: FontSize.Small
                            //color: Color.Gray
                        }
                        horizontalAlignment: HorizontalAlignment.Center
                        text: qsTr("Don't you have FDB.cz account?") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    Button {
                        preferredWidth: SizeHelper.maxWidth
                        horizontalAlignment: HorizontalAlignment.Center
                        text: qsTr("Register") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            var sheet = registerSheet.createObject()
                            sheet.open()
                        }
                    } 
                } // end of Login/Register container field container

                ListView {
                    id: listView
                    visible: root.isLogged
                    maxHeight: SizeHelper.nType ? 300 : 450
                    onCreationCompleted: {
                        model.append([{"name": qsTr("I want to see")+Retranslate.onLocaleOrLanguageChanged, 
                                    "icon": App.whiteTheme ? "asset:///images/chcem_vidiet_black.png" : "asset:///images/chcem_vidiet_white.png"},
                                {"name": qsTr("I saw")+Retranslate.onLocaleOrLanguageChanged, 
                                    "icon": App.whiteTheme ? "asset:///images/videl_som_black.png" : "asset:///images/videl_som_white.png"},
                                {"name": qsTr("Rating")+Retranslate.onLocaleOrLanguageChanged, 
                                    "icon": App.whiteTheme ? "asset:///images/hodnotil_som_black.png" : "asset:///images/hodnotil_som_white.png"}])
                    }
                    horizontalAlignment: HorizontalAlignment.Fill
                    scrollIndicatorMode: ScrollIndicatorMode.None
                    snapMode: SnapMode.None
                    flickMode: FlickMode.None
                    dataModel: ArrayDataModel {
                        id: model
                    }
                    listItemComponents: [
                        ListItemComponent {
                            type: ""
                            ListItemMyAccount {}
                        }
                    ]
                    onTriggered: {
                        clearSelection()
                        select(indexPath)
                        var item = dataModel.data(indexPath)
                        if (item){
                            console.log(indexPath)
                            var page;
                            switch (parseInt(indexPath)){
                                case 0:
                                    page = chcemVidiet.createObject()
                                    break;
                                case 1:
                                    page = videlSom.createObject()
                                    break;
                                case 2:
                                    page = hodnotilSom.createObject()
                                    break;
                            }
                            page.title = item.name
                            activePane.push(page)
                        }
                    }
                } // end of listView
                
            } // end of root container
        } // end of ScrollView
    }
}
import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    MyPage {
        id: root
        
        function request(response){
            model.clear()
            var json = JSON.parse(response)
            model.insertList(json.data)
            App.loading = false
            if (!model.size()){
            	toast.body = qsTr("Not found") + Retranslate.onLocaleOrLanguageChanged
                toast.show()
            }
        }
        
        header: TitleHeader {
            id: titleHeader
            titleText: activeTab.title
            subtitleText: activeTab.description
        }
        loading: App.loading
        loadingText: qsTr("Searching...") + Retranslate.onLocaleOrLanguageChanged
        onActiveChanged: {
            App.hladatDone.connect(request)
            if (active){
                searchField.requestFocus()
            }
        }
        
        attachedObjects: [
            ComponentDefinition {
                id: movieDetail
                source: "../pages/DetailFilmu.qml"
            },
            ComponentDefinition {
                id: actorDetail
                source: "../pages/DetailHerca.qml"
            }
        ]
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            TextField {
                id: searchField
                bottomMargin: 0
                hintText: titleHeader.titleText
                inputMode: TextFieldInputMode.Text
                autoFit: TextAutoFit.Default
                input.masking: TextInputMasking.Masked
                input.submitKey: SubmitKey.Search
                maximumLength: 40
                onTextChanging: {
                    if (!text.length) model.clear()
                }
                backgroundVisible: true
                input.onSubmitted: {
                    if (text.length){
                        App.loading = true
                        if (searchType.selectedIndex == 0){
                            Api.hladatFilm(text.trim(), true, Settings.searchLimit)
                        }else{
                            Api.hladatHerca(text.trim(), true, Settings.searchLimit)
                        }
                    }
                }
            }
            
            SegmentedControl {
                id: searchType
                topMargin: 0
                bottomMargin: 0
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: qsTr("Movie") + Retranslate.onLocaleOrLanguageChanged
                        value: "movie"
                    },
                    Option {
                        text: qsTr("Person") + Retranslate.onLocaleOrLanguageChanged
                        value: "actor"
                    }
                ]
                onSelectedOptionChanged: {
                    if (searchField.text.length){
                        App.loading = true
                        if (selectedIndex == 0){
                            Api.hladatFilm(searchField.text.trim(), true, Settings.searchLimit)
                        }else{
                            Api.hladatHerca(searchField.text.trim(), true, Settings.searchLimit)
                        }
                    }
                }
            } // end of segment control
            
            ListView {
                opacity: App.loading || root.noContentVisible ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                function itemType(data, indexPath){
                    return searchType.selectedValue
                }
                
                dataModel: GroupDataModel {
                    id: model
                    grouping: ItemGrouping.None
                    sortingKeys: ["year", "title"]
                    sortedAscending: true
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "movie"
                        ListItemHladatFilm {}
                    },
                    ListItemComponent {
                        type: "actor"
                        ListItemHladatHerca {}
                    }
                ]
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    var page;
                    if (item){
                        if (searchType.selectedIndex == 0){
                            page = movieDetail.createObject()
                            page.title = item.title
                            page.filmId = item.id
                        }else{
                        	page = actorDetail.createObject()
                            page.title = item.name.concat(" ").concat(item.lastname)
                        	page.actorId = item.id                            
                        }
                        activePane.push(page)
                    }
                }
            }
        }
    
    }
}
import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../items"
import "../actions"
import "../pages"

NaviPane {
    id: navigationPane
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    MyPage {
        id: root
        
        
        function request(item){
            var timeList = item.start.split(",")
            if (timeList.length > 1){
                timeList.sort(function(a,b){return a.split(":")[0]-b.split(":")[0]})
                item.start = timeList.join(", ")
            }
            model.insert(item)
        }
        
        header: TitleHeader {
            titleText: activeTab.title
            subtitleText: activeTab.description
        }
        loading: App.loading
        onCreationCompleted: {
            App.kinoProgramOblubeneDone.connect(request)
        }
        onActiveChanged: {
            if (active){
                App.loading = true
                Api.kinoVysielanieOblubene(Database.cinemasId())
            }
        }
        
        attachedObjects: [
            ComponentDefinition {
                id: movieDetail
                source: "../pages/DetailFilmu.qml"
            },
            SystemListDialog {
                id: alarmDialog
                property variant timeList
                property variant item
                title: qsTr("Add notify") + Retranslate.onLocaleOrLanguageChanged
                selectionMode: ListSelectionMode.Single
                confirmButton.label: qsTr("Add") + Retranslate.onLocaleOrLanguageChanged
                cancelButton.label: qsTr("Cancel") + Retranslate.onLocaleOrLanguageChanged
                onFinished: {
                    if (value == SystemUiResult.ConfirmButtonSelection){
                        var alarms = item.start.split(",")
                        var dateTime = Helper.getDate(daySelection.selectedIndex) + " " + alarms[selectedIndices].trim()
                        var timeStamp = Helper.getTimestamp(dateTime)
                        console.log("Date Time: ".concat(dateTime))
                        console.debug("Timestamp: ".concat(Helper.getTimestamp(dateTime)))
                        var data = {"title": item.title, "body": item.kinoName, "location": item.kinoAdress, "guid": timeStamp}
                        Calendar.addCinemaEvent(timeStamp, "30", data)
                    }
                }
            }
        ]
        
        actions: [
            RefreshAction {
                ActionBar.placement: ActionBarPlacement.OnBar
                onClicked: {
                    App.loading = true
                    model.clear()
                    Api.kinoVysielanieOblubene(Database.cinemasId(), Helper.getDate(daySelection.selectedIndex))
                }
            }
        ]
        
        
        Container {
            SegmentedControl {
                id: daySelection
                horizontalAlignment: HorizontalAlignment.Fill
                bottomMargin: 0
                options: [
                    Option {
                        text: qsTr("Today") + Retranslate.onLocaleOrLanguageChanged
                    },
                    Option {
                        text: qsTr("Tomorrow") + Retranslate.onLocaleOrLanguageChanged
                    },
                    Option {
                        text: qsTr("Day after tomorrow") + Retranslate.onLocaleOrLanguageChanged
                    }
                ]
                onSelectedOptionChanged: {
                    App.loading = true
                    model.clear()
                    Api.kinoVysielanieOblubene(Database.cinemasId(), Helper.getDate(daySelection.selectedIndex))
                }
            }
            
            ListView {
                id: listView
                
                function addNotify(indexPath){
                    var item = model.data(indexPath)
                    console.log("TIMES: ".concat(item.start))
                    console.log("ALARMS: ".concat(item.alarm))
                    var list = item.start.split(",")
                    alarmDialog.timeList = []
                    alarmDialog.clearList()
                    alarmDialog.timeList = list
                    alarmDialog.item = item
                    if (list.length > 1){
                        for (var item in list){
                            alarmDialog.appendItem(list[item].trim())
                        }
                        alarmDialog.show()
                    }else{
                        var dateTime = Helper.getDate(daySelection.selectedIndex) + " " + item.start
                        var timeStamp = Helper.getTimestamp(dateTime)
                        console.log("Date Time: ".concat(dateTime))
                        console.debug("Timestamp: ".concat(timeStamp))
                        var data = {"title": item.title, "body": item.kinoName, "location": item.kinoAdress, "guid": timeStamp}
                        Calendar.addCinemaEvent(timeStamp, "30", data)
                    }
                }
                
                scrollRole: ScrollRole.Main
                opacity: App.loading ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Center
                dataModel: GroupDataModel {
                    id: model
                    grouping: ItemGrouping.ByFullValue
                    sortingKeys: ["kinoName"]
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        ListItemKinoHraju {}
                    }
                ]
                layout: StackListLayout {
                    headerMode: ListHeaderMode.Sticky
                }
                bufferedScrollingEnabled: true
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    if (item && item.id != 0){
                        var page = movieDetail.createObject()
                        page.title = item.title
                        page.filmId = item.id
                        activePane.push(page)
                    }
                }
            }
        }
    
    }
}
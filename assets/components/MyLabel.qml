import bb.cascades 1.2


Label {
    id: root
    property string role: "title" // title, subtitle, highlight
    property variant color: null
    property int increase: 0
    
    
    textStyle{
        fontSize: FontSize.PointValue
        fontSizeValue: {
            if (role === "title"){
                return Qt.Settings.fontSizeTitle
            }else if (role === "subtitle"){
                return Qt.Settings.fontSizeSubtitle
            }else if (role === "highlight"){
                return Qt.Settings.fontSizeSubtitle+2
            }
        }
        //fontWeight: (role === "highlight") ? FontWeight.Bold : FontWeight.Normal
        fontFamily: (role === "title") ? Qt.Settings.fontFamilyTitle : Qt.Settings.fontFamilySubtitle
        color: color
    }
}
import bb.cascades 1.2
import com.devpda.tools 1.2


Dialog {
    id: root
    
    property alias title: titleLabel.text
    property alias value: rating.rating
    
    signal saved(int rating)
    signal canceled();
    
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            repeatPattern: RepeatPattern.X
            imageSource: "asset:///images/262626.png"
        }
    ]
    
    Container {
        preferredHeight: SizeHelper.maxHeight
        preferredWidth: SizeHelper.maxWidth
        background: Color.create(0.0, 0.0, 0.0, 0.5)
        layout: DockLayout{}
        
        Container {
            id: container
            
            layout: DockLayout{}
            preferredWidth: SizeHelper.maxWidth-150
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            background: App.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                Container {
                    preferredHeight: SizeHelper.nType ? 90 : 110
                    layout: DockLayout {}
                    background: back.imagePaint
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    
                    Container {
                        layout: DockLayout{}
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Bottom
                        preferredHeight: 10
                        background: Color.create("#".concat(Settings.activeColor))
                    }
                    
                    
                    Label {
                        id: titleLabel
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        text: qsTr("Rate movie") + Retranslate.onLocaleOrLanguageChanged
                        textStyle.color: Color.White
                        textStyle.base: SystemDefaults.TextStyles.TitleText
                        bottomMargin: 0
                    }
                }
                Container {
                    topMargin: 20
                    preferredHeight: 170
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    leftPadding: 15
                    rightPadding: 15
                    Label {
                        id: label
                        topMargin: 0
                        horizontalAlignment: HorizontalAlignment.Center
                        text: (rating.rating)+"/10"
                    }
                    MyRating {
                        id: rating
                        horizontalAlignment: HorizontalAlignment.Center
                        rating: 5
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    Slider {
                        id: slider
                        horizontalAlignment: HorizontalAlignment.Center
                        fromValue: 0
                        toValue: 10
                        value: rating.rating
                        onImmediateValueChanged: {
                            label.text = Math.floor(immediateValue)+"/10"
                            rating.rating = (immediateValue)
                        }
                    }
                    bottomMargin: 20
                } // end of content container
                Container {
                    topMargin: 0
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Bottom
                    leftPadding: 10
                    rightPadding: 10
                    bottomPadding: 10
                    Button {
                        text: qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            root.canceled()
                            close()
                        }
                    }
                    Button {
                        text: qsTr("Save") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            root.saved(parseInt(rating.rating));
                            close()
                        }
                    }
                } // end of buttons
            } // end of stacklayout container
        }
    }
}
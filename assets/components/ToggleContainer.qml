import bb.cascades 1.2


Container {
    id: root
    property alias text: label.text
    property alias checked: toggle.checked
    
    signal clicked(bool checked)
    
    horizontalAlignment: HorizontalAlignment.Fill
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    
    Label {
        id: label
        verticalAlignment: VerticalAlignment.Center
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }
    }
    
    ToggleButton {
        id: toggle
        layoutProperties: StackLayoutProperties {
            spaceQuota: -1
        }
        onCheckedChanged: {
            root.clicked(checked)
        }
    }
} // End of jednotny cas
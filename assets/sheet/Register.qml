import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../items"


Sheet {
    id: sheet
    
    
    signal registerClicked(string username, string email)
    
    
    onClosed: {
        destroy()
    }
    
    onOpened: {
        usernameField.requestFocus()
    }
    
    MyPage {
        id: root
        
        
        function register(){
            if (usernameField.text.length && userEmailField.text.length){
                sheet.registerClicked(usernameField.text.trim(), userEmailField.text.trim())
                sheet.close()
            }else{
                if (!usernameField.text.length){
                    toast.body = qsTr("You must fill username to register") + Retranslate.onLocaleOrLanguageChanged
                    usernameField.requestFocus()
                }else if (!userEmailField.text.length){
                    toast.body = qsTr("You must fill email to register") + Retranslate.onLocaleOrLanguageChanged
                    userEmailField.requestFocus()
                }
                toast.show()
            }
        }
        
        header: TitleHeader {
            id: titleHeader
            titleText: qsTr("Register an account") + Retranslate.onLocaleOrLanguageChanged
        }
        loading: App.loading
        
        attachedObjects: [
            SystemToast {
                id: toast
                button.label: ""
                autoUpdateEnabled: true
            }
        ]
        
        ScrollView {
            enabled: !App.loading
            scrollRole: ScrollRole.Main
            scrollViewProperties.pinchToZoomEnabled: false
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Container {
                leftPadding: 20
                rightPadding: 20
                topPadding: 20
                bottomPadding: 20
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                
                ImageView {
                    preferredHeight: 150
                    leftMargin: 50
                    rightMargin: 50
                    topMargin: 50
                    bottomMargin: 50
                    scalingMethod: ScalingMethod.AspectFit
                    imageSource: App.whiteTheme ? "asset:///images/fdbcz_white.png" : "asset:///images/fdbcz_black.png"
                    horizontalAlignment: HorizontalAlignment.Center
                }
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("What do you get registration") + Retranslate.onLocaleOrLanguageChanged
                }
                
                Label {
                    multiline: true
                    text: qsTr("register_description") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        fontSize: FontSize.Small
                    }
                }
                
                TextField {
                    id: usernameField
                    hintText: qsTr("username") + Retranslate.onLocaleOrLanguageChanged
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    input.onSubmitted: {
                        userEmailField.requestFocus()
                    }
                }
                
                TextField {
                    id: userEmailField
                    hintText: "email"
                    inputMode: TextFieldInputMode.EmailAddress
                    input.submitKey: SubmitKey.EnterKey
                    input.onSubmitted: {
                        root.register()
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    Button {
                        preferredWidth: SizeHelper.maxWidth
                        text: qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            sheet.close()
                        }
                    }
                    
                    Button {
                        preferredWidth: SizeHelper.maxWidth
                        text: qsTr("Register") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            root.register()
                        }
                    }
                } // end of buttons containers
                

                Label {
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("terms and conditions of FDB.cz") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.create("#"+Settings.activeColor)
                        fontWeight: FontWeight.Bold
                    }
                    onTouch: {
                        Helper.invokeBrowser("http://www.fdb.cz/registrace.html?sek=3")
                    }
                }
            
            } // end of container
        }// end of scroll view
    } // end of MyPage
}
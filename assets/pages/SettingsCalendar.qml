import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../items"

MyPage {
    id: root
    
    
    function kinoReminderText(value){
        var den = qsTr(" day") + Retranslate.onLocaleOrLanguageChanged
        var dni = qsTr(" days") + Retranslate.onLocaleOrLanguageChanged
        var valueString = new String(value)
        switch (value){
            case -1:
                kinoReminderLabel.text =  valueString.replace("-", "").concat(den)
                break;
            default:
                kinoReminderLabel.text = valueString.replace("-", "").concat(dni)
                break;
        }
    }
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            Header {
                title: qsTr("Notify") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Fill
                    topMargin: 0
                    multiline: true
                    text: qsTr("calendar_manualnotify_desc") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        color: Color.Gray
                    
                    }
                }
                
                ToggleContainer {
                    text: qsTr("Add manually") + Retranslate.onLocaleOrLanguageChanged
                    checked: Settings.manualNotify
                    onClicked: {
                        Settings.manualNotify = checked
                    }
                } 
            } // End of Manual notify
            
            Header {
                title: qsTr("Warn program") + Retranslate.onLocaleOrLanguageChanged
                //subtitle: Settings.reminder + " min"
            }
            
            Container {
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                
                
                ToggleContainer {
                    enabled: !Settings.manualNotify
                    opacity: !Settings.manualNotify ? 1 : 0.5
                    text: qsTr("The single alarm time") + Retranslate.onLocaleOrLanguageChanged
                    checked: Settings.unitedTime
                    onClicked: {
                        console.log("UnitedTime: ".concat(checked))
                        Settings.unitedTime = checked
                    }
                } // end of jednotny cas
                
                Divider { }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        opacity: Settings.unitedTime ? 1 : 0.5
                        multiline: true
                        text: qsTr("Warn before tv program") + Retranslate.onLocaleOrLanguageChanged
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    Label {
                        opacity: Settings.unitedTime ? 1 : 0.5
                        id: reminderLabel
                        text: Settings.reminder + " min"
                        textStyle{
                            color: Color.create("#".concat(Settings.activeColor))
                        }
                    }
                }
                
                
                Slider {
                    id: slider
                    enabled: Settings.unitedTime
                    toValue: 60.0
                    value: Settings.reminder
                    onImmediateValueChanged: {
                        reminderLabel.text = Math.floor(immediateValue) + " min"
                    }
                    onValueChanged: {
                        console.log("onValueChanged, saving reminder")
                        Settings.reminder = value
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    Label {
                        opacity: Settings.unitedTime ? 1 : 0.5
                        multiline: true
                        text: qsTr("Warn before cinema program") + Retranslate.onLocaleOrLanguageChanged
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    Label {
                        opacity: Settings.unitedTime ? 1 : 0.5
                        id: kinoReminderLabel
                        text: kinoReminderText(Settings.kinoReminder)
                        textStyle{
                            color: Color.create("#".concat(Settings.activeColor))
                        }
                    }
                }
                
                
                Slider {
                    id: kinoSlider
                    enabled: Settings.unitedTime
                    fromValue: -1
                    toValue: -5.0
                    value: Settings.kinoReminder
                    onImmediateValueChanged: {
                        kinoReminderText(Math.floor(immediateValue))
                    }
                    onValueChanged: {
                        console.log("onValueChanged, saving  kino reminder, value = ".concat(value))
                        Settings.kinoReminder = value
                    }
                }
                
            } // end of jednotny cas upozornenia
        }
    }
}
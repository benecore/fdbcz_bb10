import bb.cascades 1.2
import bb.device 1.2
import bb 1.0
import com.devpda.tools 1.2
import bb.platform 1.2

import "../components"
import "../items"

MyPage {
    
    header: TitleHeader {
        id: titleHeader
        titleText: aboutAction.title
    }
    loading: false
    
    property bool working: false
    attachedObjects: [
        ApplicationInfo {
            id: appInfo
        },
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Invocation failed")
            }
        },
        PaymentManager{
            id: payManager
            onPurchaseFinished: {
                if (reply.errorCode == 0){
                    toast.body = qsTr("Thanks for support") + Retranslate.onLanguageChanged
                    toast.show()
                }else{
                    console.log("ERROR PAYMENT: ".concat(reply.errorText))
                }
                working = false
            }
        }
    ]
    
    onActiveChanged: {
        if (active){
            payManager.setConnectionMode(1) // 1 = production | 0 - test mode
            console.log("onActiveChanged, About.qml, true")
            aboutAction.enabled = false
        }else{
            console.log("onActiveChanged, About.qml, false")
            aboutAction.enabled = true
        }
    }
    
    Container {
        layout: DockLayout {}
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        ImageView {
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Bottom
            imageSource: "asset:///images/devpda.png"
            opacity: 0.5
        }// end of Logo DevPDA
        
        
        ScrollView {
            opacity: working ? 0.5 : 1
            scrollRole: ScrollRole.Main
            scrollViewProperties.pinchToZoomEnabled: false
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            Container {
                
                
                Container {
                    topPadding: 20
                    leftPadding: 20
                    rightPadding: 20
                    bottomPadding: 20
                    horizontalAlignment: HorizontalAlignment.Fill
                    Label {
                        horizontalAlignment: HorizontalAlignment.Center
                        text: "FDB.cz"
                        textStyle{
                            base: SystemDefaults.TextStyles.BigText
                            fontWeight: FontWeight.Bold
                        }
                        bottomMargin: 0
                    }
                    
                    Label {
                        topMargin: 0
                        horizontalAlignment: HorizontalAlignment.Center
                        text: qsTr("Version: %1").arg(appInfo.version)
                        bottomMargin: 0
                    }
                    
                    
                    Label {
                        topMargin: 0
                        horizontalAlignment: HorizontalAlignment.Fill
                        multiline: true
                        textStyle{
                            fontStyle: FontStyle.Italic
                        }
                        text: qsTr("about_app_description") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    Divider {
                        topMargin: 5
                        bottomMargin: 5
                    }
                    
                    Label {
                        topMargin: 0
                        horizontalAlignment: HorizontalAlignment.Center
                        multiline: true
                        textStyle{
                            fontStyle: FontStyle.Italic
                            color: Color.Gray
                        }
                        text: qsTr("If you like the app support its further development.") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Center
                        
                        Button {
                            text: "$0.99"
                            imageSource: "asset:///images/donations/small_donations.png"
                            onClicked: {
                                working = true
                                payManager.requestPurchase("","FDBcz-small","FDB.cz support")
                            }
                        }
                        
                        Button {
                            text: "$1.99"
                            imageSource: "asset:///images/donations/medium_donations.png"
                            onClicked: {
                                working = true
                                payManager.requestPurchase("","FDBcz-medium","FDB.cz support")
                            }
                        }
                        
                        Button {
                            text: "$2.99"
                            imageSource: "asset:///images/donations/large_donations.png"
                            onClicked: {
                                working = true
                                payManager.requestPurchase("","FDBcz-large","FDB.cz support")
                            }
                        }
                    } // end of payment
                
                } // End of title version container
                
                Header {
                    title: qsTr("Developer") + Retranslate.onLocaleOrLanguageChanged
                }
                Container {
                    topPadding: 20
                    leftPadding: 20
                    rightPadding: 20
                    bottomPadding: 20
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    Label {
                        text: "Zoltán 'Benecore' Benke"
                        bottomMargin: 0
                        textStyle{
                            base: SystemDefaults.TextStyles.TitleText
                        }
                    }
                    
                    Label {
                        textStyle{
                            color: Color.create("#".concat(Settings.activeColor))
                            fontWeight: FontWeight.Bold
                        }
                        topMargin: 0
                        text: "www.devpda.net"
                        onTouch: {
                            Helper.invokeBrowser("http://".concat(text))
                        }
                        bottomMargin: 0
                    }
                    
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        
                        Label {
                            verticalAlignment: VerticalAlignment.Center
                            text: qsTr("In case of any questions or ideas, feel free to contact me") + Retranslate.onLocaleOrLanguageChanged
                            multiline: true
                            textStyle{
                                fontStyle: FontStyle.Italic
                            }
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        }
                        
                        Button {
                            maxHeight: 70
                            maxWidth: 70
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: App.whiteTheme ? "asset:///images/contact_me_black.png" : "asset:///images/contact_me.png"
                            onClicked: {
                                invoker.target = "sys.pim.uib.email.hybridcomposer"
                                invoker.action = "bb.action.SENDEMAIL"
                                invoker.uri = "mailto:support@devpda.net?subject=FDB.cz (BB10)"
                                invoker.invoke()
                            }
                        }
                    } // end of Contact container
                } // End of developer container
                
                Header {
                    title: qsTr("Data povides") + Retranslate.onLocaleOrLanguageChanged
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    topPadding: 20
                    leftPadding: 20
                    rightPadding: 20
                    bottomPadding: 20
                    
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        Label {
                            text: "Filmová databáze s.r.o."
                            bottomMargin: 0
                            textStyle{
                                base: SystemDefaults.TextStyles.TitleText
                            }
                        }
                        Label {
                            textStyle{
                                color: Color.create("#".concat(Settings.activeColor))
                                fontWeight: FontWeight.Bold
                            }
                            topMargin: 0
                            text: "www.fdb.cz"
                            onTouch: {
                                Helper.invokeBrowser("http://".concat(text))
                            }
                            bottomMargin: 0
                        }
                    }
                    Button {
                        topMargin: 5
                        maxHeight: 70
                        maxWidth: 70
                        horizontalAlignment: HorizontalAlignment.Center
                        imageSource: App.whiteTheme ? "asset:///images/contact_me_black.png" : "asset:///images/contact_me.png"
                        onClicked: {
                            invoker.target = "sys.pim.uib.email.hybridcomposer"
                            invoker.action = "bb.action.SENDEMAIL"
                            invoker.uri = "mailto:info@fdb.cz?subject=FDB.cz (BB10)"
                            invoker.invoke()
                        }
                    }
                } // end of Data zabezpecuje
                
                Header {
                    title: qsTr("Thanks") + Retranslate.onLocaleOrLanguageChanged
                }
                
                Container {
                    topPadding: 20
                    leftPadding: 20
                    rightPadding: 20
                    bottomPadding: 20
                    horizontalAlignment: HorizontalAlignment.Fill
                    
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        
                        Label {
                            verticalAlignment: VerticalAlignment.Center
                            text: "Tomáš 'Owes' Ovísek"
                            textStyle{
                                base: SystemDefaults.TextStyles.TitleText
                            }
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        }
                        
                        Button {
                            maxHeight: 70
                            maxWidth: 70
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: App.whiteTheme ? "asset:///images/twitter_black.png" : "asset:///images/twitter_white.png"
                            onClicked: {
                                Helper.invokeBrowser("https://twitter.com/owes89")
                            }
                        }
                    } // end of Owes container
                    
                    Divider {
                        topMargin: 0
                        bottomMargin: 0
                    }
                    
                    Container {
                        topMargin: 0
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        
                        Label {
                            verticalAlignment: VerticalAlignment.Center
                            text: "Vít 'Ketiv666' Novák"
                            textStyle{
                                base: SystemDefaults.TextStyles.TitleText
                            }
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                        }
                        
                        Button {
                            topMargin: 0
                            maxHeight: 70
                            maxWidth: 70
                            horizontalAlignment: HorizontalAlignment.Center
                            imageSource: App.whiteTheme ? "asset:///images/twitter_black.png" : "asset:///images/twitter_white.png"
                            onClicked: {
                                Helper.invokeBrowser("https://twitter.com/Ketiv666")
                            }
                        }
                    } // end of Ketiv666
                }
            }
        } // end of scroll view
    }
}
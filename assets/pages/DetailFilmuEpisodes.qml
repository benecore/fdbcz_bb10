import bb.cascades 1.2
import com.devpda.items 1.2
import com.devpda.tools 1.2

import "../components"
import "../items"


Container {
    id: root
    
    property MovieItem movie: null
    onMovieChanged: {
        for (var item in movie.epizody){
            episodesModel.insert(movie.epizody[item])
        }
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: movieDetail
            source: "DetailFilmu.qml"
        }
    ]
    
    ListView {
        id: episodesList
        scrollRole: ScrollRole.Main
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: episodesModel
            grouping: ItemGrouping.ByFullValue
            sortingKeys: ["year"]
            sortedAscending: false
        }
        listItemComponents: [
            ListItemComponent {
              	type: "header"
              	Header {
                   title: qsTr("Year") + Retranslate.onLocaleOrLanguageChanged
                   subtitle: ListItemData
               }  
            },
            ListItemComponent {
                type: "item"
                ListItemEpisodes {}
            }
        ]
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        }
        onTriggered: {
            var item = dataModel.data(indexPath)
            if (item){
                var page = movieDetail.createObject()
                page.title = item.title
                page.filmId = item.id
                activePane.push(page)
            }
        }
    }
}
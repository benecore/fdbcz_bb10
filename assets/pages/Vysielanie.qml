import bb.cascades 1.2
import com.devpda.tools 1.2
import bb.system 1.2

import "../components"
import "../items"
import "../actions"

MyPage {
    id: root
    
    property variant stanica
    property variant scrollTo
    
    function request(response){
        var json = JSON.parse(response)
        model.insertList(json.data)
        for (var i = model.first(); i != model.last(); i = model.after(i)){
            var program = model.data(i)
            if (program.starttime < Helper.getTime() && program.endtime >= Helper.getTime()){
                console.log("Index is: ".concat(i))
                scrollTo = i;
                break;
            }
        }
        scrollTimer.start()
        App.loading = false
    }
    
    header: TitleHeader {
        id: titleHeader
        titleText: stanica ? stanica.name : ""
        imageVisible: true
        imageSource: stanica ? "http://img.fdb.cz/tv56/".concat(stanica.logo).replace(".png", "@2x.png") : ""
    }
    loading: App.loading
    onCreationCompleted: {
        App.vysielanieDone.connect(request)
    
    }
    onActiveChanged: {
        console.log("onActiveChanged")
        if (active){
            App.loading = true
            Api.vysielanie(stanica.id)
        }else{
        
        }
    }
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            enabled: (daySelection.selectedValue == Helper.getDate())
            title: qsTr("Scroll to current") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/actions/scroll_to.png"
            onTriggered: {
                listView.scrollToItem(scrollTo,ScrollAnimation.Smooth)
            }
        },
        RefreshAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                App.loading = true
                model.clear()
                Api.vysielanie(stanica.id, daySelection.selectedValue);
            }
        }
    ]
    
    
    attachedObjects: [
        Timer {
            id: scrollTimer
            interval: 5
            onTimeout: {
                if (daySelection.selectedValue == Helper.getDate()){
                    listView.scrollToItem(scrollTo, ScrollAnimation.Default)
                }
                stop()
            }
        },
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Invocation failed")
            }
        },
        ComponentDefinition {
            id: movieDetail
            source: "DetailFilmu.qml"
        },
        SystemListDialog {
            
        }
    ]
    
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        SegmentedControl {
            id: daySelection
            bottomMargin: 0
            horizontalAlignment: HorizontalAlignment.Fill
            options: [
                Option {
                    text: qsTr("Today") + Retranslate.onLocaleOrLanguageChanged
                    value: Helper.getDate()
                },
                Option {
                    text: qsTr("Tomorrow") + Retranslate.onLocaleOrLanguageChanged
                    value: Helper.getDate(1)
                },
                Option {
                    text: qsTr("Day after tomorrow") + Retranslate.onLocaleOrLanguageChanged
                    //text: Helper.getDate(2, "dd-MM")
                    value: Helper.getDate(2)
                }
            ]
            onSelectedIndexChanged: {
                App.loading = true
                model.clear()
                Api.vysielanie(stanica.id, selectedValue)
            }
        }
        
        ListView {
            id: listView
            
            function addNotify(indexPath){
                console.log("Called")
                var item = model.data(indexPath)
                var data = {"title": item.title, "body": "", "station": stanica.name, "guid": item.id}
                Calendar.addEvent(item.alarm, item.time, data)
            }
            
            scrollRole: ScrollRole.Main
            opacity: App.loading ? 0 : 1
            attachedObjects: [
                FadeTransition {
                    fromOpacity: 0
                    toOpacity: 1
                    duration: 300
                    target: listView
                }
            ]
            dataModel: GroupDataModel {
                id: model
                grouping: ItemGrouping.None
                sortedAscending: true
                sortingKeys: ["alarm", "starttime"]
            }
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemVysielanie {}
                }
            ]
            layout: StackListLayout {
                headerMode: ListHeaderMode.Sticky
            }
            bufferedScrollingEnabled: true
            onTriggered: {
                var item = dataModel.data(indexPath)
                if (item && item.film != 0){
                    var page = movieDetail.createObject()
                    page.title = item.title
                    page.filmId = item.film
                    activePane.push(page)
                }
            }
        }
    }
}

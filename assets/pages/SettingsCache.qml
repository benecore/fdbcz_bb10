import bb.cascades 1.2
import bb.system 1.2

import "../components"

MyPage {
    id: root
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    /*
     * Header {
     title: qsTr("Vyrovnávacia pamäť", "Thumbnail cache") + Retranslate.onLocaleOrLanguageChanged
     subtitle: qsTr("obrázky", "images") + Retranslate.onLocaleOrLanguageChanged
     }
     
     Label {
     text: Helper.thumbnailSize + " | " + Helper.thumbnailCount
     }
     
     Button {
     horizontalAlignment: HorizontalAlignment.Center
     text: qsTr("Vymazať", "Clear") + Retranslate.onLocaleOrLanguageChanged
     onClicked: {
     clearThumbnails.show()
     }
     attachedObjects: [
     SystemDialog {
     id: clearThumbnails
     title: qsTr("Vymazať", "Clear") + Retranslate.onLocaleOrLanguageChanged
     body: qsTr("Vymazať vyrovnávaciu pamäť obrázkov?", "Clear thumbnails cache?") + Retranslate.onLocaleOrLanguageChanged
     confirmButton{
     label: qsTr("Áno", "Yes") + Retranslate.onLocaleOrLanguageChanged
     }
     onFinished: {
     if (value == SystemUiResult.ConfirmButtonSelection){
     console.log("CLEAR THUMBNAILS: ".concat(Helper.clearThumbnails()))
     }else{
     console.log("CANCEL CLICKED")
     }
     }
     }
     ]
     }
     */
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            
            Header {
                title: qsTr("Summary") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 20
                rightPadding: 20
                topPadding: 20
                horizontalAlignment: HorizontalAlignment.Center
                Container {
                    topPadding: 5
                    bottomPadding: 5
                    leftPadding: 5
                    rightPadding: 5
                    minWidth: (SizeHelper.maxWidth - 170) / 2
                    maxWidth: (SizeHelper.maxWidth - 170) / 2
                    
                    Label {
                        textStyle.fontWeight: FontWeight.Bold
                        horizontalAlignment: HorizontalAlignment.Center
                        text: Helper.thumbnailCount
                    }
                    
                    Label {
                        text: qsTr("count") + Retranslate.onLocaleOrLanguageChanged
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                } // End of thumbnail count
                
                Container {
                    background: App.whiteTheme ? Color.Black : Color.LightGray
                    verticalAlignment: VerticalAlignment.Fill
                    horizontalAlignment: HorizontalAlignment.Left
                    maxHeight: Infinity                        
                    minWidth: 1
                    maxWidth: 1
                }
                
                Container {
                    topPadding: 5
                    bottomPadding: 5
                    leftPadding: 5
                    rightPadding: 5
                    minWidth: (SizeHelper.maxWidth - 170) / 2
                    maxWidth: (SizeHelper.maxWidth - 170) / 2
                    
                    Label {
                        textStyle.fontWeight: FontWeight.Bold
                        horizontalAlignment: HorizontalAlignment.Center
                        text: Helper.thumbnailSizeString
                    }
                    
                    Label {
                        text: qsTr("total size") + Retranslate.onLocaleOrLanguageChanged
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                } // End of thumbnail size
            } // End of Reuslt header
            
            Header {
                title: qsTr("Memory limit") + Retranslate.onLocaleOrLanguageChanged
            }
            
            
            Container {
                leftPadding: 20
                rightPadding: 20
                topPadding: 20
                
                Label {
                    textStyle{
                        base: SystemDefaults.TextStyles.SubtitleText
                        color: Color.Gray
                    }
                    text: qsTr("cache_description") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    topMargin: 0
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    Label {
                        text: qsTr("Limit") + Retranslate.onLocaleOrLanguageChanged
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                    }
                    
                    Label {
                        text: Helper.convSize(slider.immediateValue);
                    }
                }
                
                Slider {
                    id: slider
                    fromValue: 5242880
                    toValue: 104857600
                    value: Settings.cacheLimit
                    onValueChanged: {
                        Settings.cacheLimit = value
                    }
                }
            } // end of cache limit container
            
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("Clear") + Retranslate.onLocaleOrLanguageChanged
                onClicked: {
                    clearThumbnails.show()
                }
                attachedObjects: [
                    SystemDialog {
                        id: clearThumbnails
                        title: qsTr("Clear cache?") + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Are you sure you want to clear thumbnails cache?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Yes") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                console.log("CLEAR THUMBNAILS: ".concat(Helper.clearThumbnails()))
                            }else{
                                console.log("CANCEL CLICKED")
                            }
                        }
                    }
                ]
            }
        }
    }
}
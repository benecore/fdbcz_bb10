import bb.cascades 1.2
import com.devpda.tools 1.2
import bb.cascades.pickers 1.0

import "../components"
import "../items"

MyPage{
    id: root
    
    function setFontPickerTitleIndex(){
        var indexFamily = 0;
        var indexSize = 0;
        var pickerText;
        if (Settings.fontFamilyTitle === "Sans-serif"){
            indexFamily = 0
        }
        else if (Settings.fontFamilyTitle === "Serif"){
            indexFamily = 1
        }
        else if (Settings.fontFamilyTitle === "Monospace"){
            indexFamily = 2
        }
        else if (Settings.fontFamilyTitle === "Fantasy"){
            indexFamily = 3
        }
        else{
            indexFamily = 4
        }
        if (Settings.fontSizeTitle === 4){
            pickerText = "XSmall"
            indexSize = 0;
        }
        else if (Settings.fontSizeTitle === 5){
            pickerText = "Small"
            indexSize = 1;
        }
        else if (Settings.fontSizeTitle === 6){
            pickerText = "Medium"
            indexSize = 2;
        }
        else if (Settings.fontSizeTitle === 8){
            pickerText = "Large"
            indexSize = 3;
        }
        else if (Settings.fontSizeTitle === 9){
            pickerText = "XLarge"
            indexSize = 4;
        }
        else{
            pickerText = "XXLarge"
            indexSize = 5
        }
        fontPickerTitle.select(0,indexFamily,ScrollAnimation.None)
        fontPickerTitle.select(1,indexSize,ScrollAnimation.None)
        fontPickerTitle.description = Settings.fontFamilyTitle + " | " + pickerText
    }
    
    function setFontPickerSubtitleIndex(){
        var indexFamily = 0;
        var indexSize = 0;
        var pickerText;
        if (Settings.fontFamilySubtitle === "Sans-serif"){
            indexFamily = 0
        }
        else if (Settings.fontFamilySubtitle === "Serif"){
            indexFamily = 1
        }
        else if (Settings.fontFamilySubtitle === "Monospace"){
            indexFamily = 2
        }
        else if (Settings.fontFamilySubtitle === "Fantasy"){
            indexFamily = 3
        }
        else{
            indexFamily = 4
        }
        if (Settings.fontSizeSubtitle === 4){
            pickerText = "XSmall"
            indexSize = 0;
        }
        else if (Settings.fontSizeSubtitle === 5){
            pickerText = "Small"
            indexSize = 1;
        }
        else if (Settings.fontSizeSubtitle === 6){
            pickerText = "Medium"
            indexSize = 2;
        }
        else if (Settings.fontSizeSubtitle === 8){
            pickerText = "Large"
            indexSize = 3;
        }
        else if (Settings.fontSizeSubtitle === 9){
            pickerText = "XLarge"
            indexSize = 4;
        }
        else{
            pickerText = "XXLarge"
            indexSize = 5
        }
        fontPickerSubtitle.select(0,indexFamily,ScrollAnimation.None)
        fontPickerSubtitle.select(1,indexSize,ScrollAnimation.None)
        fontPickerSubtitle.description = Settings.fontFamilySubtitle + " | " + pickerText
    }
    
    function setFontBaseTitleSize(index){
        switch (index){
            case 0:
                Settings.fontSizeTitle = 4; break;
            case 1:
                Settings.fontSizeTitle = 5; break;
            case 2:
                Settings.fontSizeTitle = 6; break;
            case 3:
                Settings.fontSizeTitle = 8; break;
            case 4:
                Settings.fontSizeTitle = 9; break;
            case 5:
                Settings.fontSizeTitle = 10; break;
            default:
                break;
        }
    }
    
    
    function setFontBaseSubtitleSize(index){
        switch (index){
            case 0:
                Settings.fontSizeSubtitle = 4; break;
            case 1:
                Settings.fontSizeSubtitle = 5; break;
            case 2:
                Settings.fontSizeSubtitle = 6; break;
            case 3:
                Settings.fontSizeSubtitle = 8; break;
            case 4:
                Settings.fontSizeSubtitle = 9; break;
            case 5:
                Settings.fontSizeSubtitle = 10; break;
            default:
                break;
        }
    }
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    onCreationCompleted: {
        toast.body = qsTr("To apply changes restart application") + Retranslate.onLocaleOrLanguageChanged
        ThemeSettings.themeSaved.connect(toast.show)
        var theme = ThemeSettings.getThemeString("theme")
        console.log("THEME: ".concat(theme))
        themeDropDown.setSelectedIndex(theme == "bright" ? 0 : 1)
    }
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            
            Header {
                title: qsTr("Application theme") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    id: themeDropDown
                    
                    title: qsTr("Theme") + Retranslate.onLocaleOrLanguageChanged
                    
                    Option {
                        text: qsTr("Bright") + Retranslate.onLocaleOrLanguageChanged
                        value: VisualStyle.Bright
                    }
                    
                    Option {
                        text: qsTr("Dark") + Retranslate.onLocaleOrLanguageChanged
                        value: VisualStyle.Dark
                    }
                    onSelectedIndexChanged: {
                        ThemeSettings.setThemeString("theme", selectedOption.value === VisualStyle.Bright ? "bright" : "dark")
                    }
                }
            }
            
            Header {
                id: fontHeader
                title: qsTr("Font") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                
                Picker {
                    id: fontPickerTitle
                    
                    onCreationCompleted: {
                        setFontPickerTitleIndex()
                    }
                    title: qsTr("Title font") + Retranslate.onLocaleOrLanguageChanged
                    description: Settings.fontFamilyTitle + " | " + dataModel.data([1, fontPickerTitle.selectedIndex(1)]).text
                    dataModel: XmlDataModel {
                        source: "asset:///models/fontpicker.xml"
                    }
                    onExpandedChanged: {
                        if (SizeHelper.nType){
                            if (expanded){
                                root.actionBarVisibility = ChromeVisibility.Hidden
                            }else{
                                root.actionBarVisibility = ChromeVisibility.Default
                            }
                        }
                    }
                    pickerItemComponents: [
                        PickerItemComponent {
                            type: "family"
                            content: Container {
                                layout: DockLayout{}
                                Label {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    text: pickerItemData.text
                                }
                            }
                        },
                        PickerItemComponent {
                            type: "size"
                            content: Container {
                                layout: DockLayout{}
                                Label {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    text: pickerItemData.text
                                }
                            }
                        }
                    ] // end of pickerItemComponents
                    onSelectedValueChanged: {
                        if (selectedValue){
                            description = Settings.fontFamilyTitle + " | " + dataModel.data([1, fontPickerTitle.selectedIndex(1)]).text
                            Settings.fontFamilyTitle = dataModel.data([0, fontPickerTitle.selectedIndex(0)]).text
                            setFontBaseTitleSize(fontPickerTitle.selectedIndex(1));
                            //Settings.fontSizeTitle = dataModel.data([1, fontPickerTitle.selectedIndex(1)]).text
                            //console.debug(Settings.fontFamilyTitle + ", " + Settings.fontSizeTitle)
                        }
                    }
                } // end of Title picker
                
                Picker {
                    id: fontPickerSubtitle
                    topMargin: 10
                    onCreationCompleted: {
                        setFontPickerSubtitleIndex()
                    }
                    title: qsTr("Subtitle font") + Retranslate.onLocaleOrLanguageChanged
                    dataModel: XmlDataModel {
                        source: "asset:///models/fontpicker.xml"
                    }
                    onExpandedChanged: {
                        if (SizeHelper.nType){
                            if (expanded){
                                root.actionBarVisibility = ChromeVisibility.Hidden
                            }else{
                                root.actionBarVisibility = ChromeVisibility.Default
                            }
                        }
                    }
                    pickerItemComponents: [
                        PickerItemComponent {
                            type: "family"
                            content: Container {
                                layout: DockLayout{}
                                Label {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    text: pickerItemData.text
                                }
                            }
                        },
                        PickerItemComponent {
                            type: "size"
                            content: Container {
                                layout: DockLayout{}
                                Label {
                                    horizontalAlignment: HorizontalAlignment.Center
                                    verticalAlignment: VerticalAlignment.Center
                                    text: pickerItemData.text
                                }
                            }
                        }
                    ] // end of pickerItemComponents
                    onSelectedValueChanged: {
                        if (selectedValue){
                            description = Settings.fontFamilySubtitle + " | " + dataModel.data([1, fontPickerSubtitle.selectedIndex(1)]).text
                            Settings.fontFamilySubtitle = dataModel.data([0, fontPickerSubtitle.selectedIndex(0)]).text
                            setFontBaseSubtitleSize(fontPickerSubtitle.selectedIndex(1))
                            //Settings.fontSizeSubtitle = dataModel.data([1, fontPickerSubtitle.selectedIndex(1)]).text
                            //console.debug(Settings.fontFamilySubtitle + ", " + Settings.fontSizeSubtitle)
                        }
                    }
                } // End of Subtitle font picker
            } // End of Font pickers container
            
            Header {
                title: fontHeader.title
                subtitle: qsTr("Preview") + Retranslate.onLocaleOrLanguageChanged
            }
            
            
            ListItemPraveBeziPreview {
                topPadding: 5
                bottomPadding: 5
                titleText: qsTr("Title font") + Retranslate.onLocaleOrLanguageChanged
                subtitle1: qsTr("Subtitle font") + Retranslate.onLocaleOrLanguageChanged
                subtitle2: subtitle1
            }
            
            
            Header {
                title: qsTr("Highlight color") + Retranslate.onLocaleOrLanguageChanged
            }
            
            ListView {
                id: listView
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                onCreationCompleted: {
                    dataModel.append(["0098f0", "96b800", "cc3f10", "660099", "ff0080", "ffcc00"])
                    preferredHeight = 130
                }
                dataModel: ArrayDataModel {
                    id: model
                }
                layout: GridListLayout {
                    columnCount: 6
                    
                    horizontalCellSpacing: 10
                    verticalCellSpacing: 10
                    orientation: LayoutOrientation.TopToBottom
                    cellAspectRatio: 1
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ImageView {
                            id: image
                            scaleX: (Qt.Settings.activeColor === ListItemData) ? 0.7 : 1
                            scaleY: scaleX
                            imageSource: "asset:///images/colors/".concat(ListItemData).concat(".png")
                            scalingMethod: ScalingMethod.AspectFit
                        }
                    }
                ]
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    Settings.activeColor = item;
                }
                scrollIndicatorMode: ScrollIndicatorMode.None
                layoutProperties: StackLayoutProperties{
                    spaceQuota: -1
                }
            } // End of ListView
            
            /*
             Picker {
             id: fontPickerTitle
             onCreationCompleted: {
             setFontPickerTitleIndex()
             }
             title: qsTr("Title font") + Retranslate.onLocaleOrLanguageChanged
             description: Settings.fontFamilyTitle + " | " + dataModel.data([1, fontPickerTitle.selectedIndex(1)]).text
             dataModel: XmlDataModel {
             source: "asset:///models/fontpicker.xml"
             }
             expanded: false
             pickerItemComponents: [
             PickerItemComponent {
             type: "family"
             content: Container {
             layout: DockLayout{}
             Label {
             horizontalAlignment: HorizontalAlignment.Center
             verticalAlignment: VerticalAlignment.Center
             text: pickerItemData.text
             }
             }
             },
             PickerItemComponent {
             type: "size"
             content: Container {
             layout: DockLayout{}
             Label {
             horizontalAlignment: HorizontalAlignment.Center
             verticalAlignment: VerticalAlignment.Center
             text: pickerItemData.text
             }
             }
             }
             ] // end of pickerItemComponents
             onSelectedValueChanged: {
             if (selectedValue){
             description = Settings.fontFamilyTitle + " | " + dataModel.data([1, fontPickerTitle.selectedIndex(1)]).text
             Settings.fontFamilyTitle = dataModel.data([0, fontPickerTitle.selectedIndex(0)]).text
             setFontBaseTitleSize(fontPickerTitle.selectedIndex(1));
             //Settings.fontSizeTitle = dataModel.data([1, fontPickerTitle.selectedIndex(1)]).text
             //console.debug(Settings.fontFamilyTitle + ", " + Settings.fontSizeTitle)
             }
             }
             } // end of Title picker
             */
        }
    }
}
import bb.cascades 1.2
import com.devpda.tools 1.2
import bb.system 1.2

import "../components"
import "../items"
import "../actions"

MyPage {
    id: root
    
    
    function request(response){
        var json = JSON.parse(response)
        for (var item in json.data){
            var obj = json.data[item]
            var timeList = obj.time.split(",")
            if (timeList.length > 1){
                timeList.sort(function(a,b){return a.split(":")[0]-b.split(":")[0]})
                obj.time = timeList.join(", ")
            }
            model.insert(obj)
        }
        kino = json
        App.loading = false
    }
    
    
    property variant kino
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    property string kinoId
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: App.loading
    
    
    onActiveChanged: {
        if (active){
            App.loading = true
            App.kinoProgramDone.connect(request)
            Api.kinoProgram(kinoId)
        }
    }
    
    
    actions: [
        RefreshAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                App.loading = true
                model.clear()
                Api.kinoProgram(kinoId)
            }
        },
        ShowMapAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                var map = mapa.createObject()
                map.title = kino.name
                map.subtitle = kino.adress
                var location = {}
                location.lat = kino.lat
                location.lon = kino.lon
                map.location = location
                activePane.push(map)
            }
        }
    ]
    
    attachedObjects: [
        ComponentDefinition {
          	id: mapa
          	source: "Mapa.qml"  
        },
        ComponentDefinition {
            id: detailFilmu
            source: "../pages/DetailFilmu.qml"
        },
        SystemListDialog {
            id: alarmDialog
            property variant timeList
            property variant item
            title: qsTr("Add notify") + Retranslate.onLocaleOrLanguageChanged
            selectionMode: ListSelectionMode.Single
            confirmButton.label: qsTr("Add") + Retranslate.onLocaleOrLanguageChanged
            cancelButton.label: qsTr("Cancel") + Retranslate.onLocaleOrLanguageChanged
            onFinished: {
                if (value == SystemUiResult.ConfirmButtonSelection){
                    var alarms = item.alarm.split(";")
                    console.log("Start: ".concat(alarms[selectedIndices]))
                    var data = {"title": item.film, "body": kino.name, "location": kino.adress, "guid": item.idz}
                    Calendar.addCinemaEvent(alarms[selectedIndices], "30", data)
                }
            }
        }
    ]
    
    
    ListView {
        id: listView
        
        function addNotify(indexPath){
            var item = model.data(indexPath)
            // {"title": Nazov filmu, "body": Popis filmu, "station": Nazov stanice, "guid": Unikatne ID}
            console.log("TIMES: ".concat(item.time))
            console.log("ALARMS: ".concat(item.alarm))
            var list = item.time.split(",")
            alarmDialog.timeList = []
            alarmDialog.clearList()
            alarmDialog.timeList = list
            alarmDialog.item = item
            if (list.length > 1){
                for (var item in list){
                    alarmDialog.appendItem(list[item].trim())
                }
                alarmDialog.show()
            }else{
                var data = {"title": item.film, "body": kino.name, "location": kino.adress, "guid": item.idz}
                Calendar.addCinemaEvent(item.alarm, "30", data)
            }
            
            //var data = {"title": item.film.title, "body": item.film.zanry, "station": item.name, "guid": item.idz}
            //Calendar.addEvent(item.alarm, item.min, data)
        }
        
        scrollRole: ScrollRole.Main
        opacity: App.loading ? 0 : 1
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: model
            grouping: ItemGrouping.ByFullValue
            sortingKeys: ["date", "alarm", "time", "film"]
            sortedAscending: true
        }
        listItemComponents: [
            ListItemComponent {
                type: "header"
                Header {
                    title: ListItemData
                }  
            },
            ListItemComponent {
                type: "item"
                ListItemKinoProgram {}
            }
        ]
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        }
        bufferedScrollingEnabled: true
        onTriggered: {
            var item = dataModel.data(indexPath)
            if (item && item.id != 0){
                var page = detailFilmu.createObject()
                page.title = item.film
                page.filmId = item.id
                activePane.push(page);
            }
        }
    }
}
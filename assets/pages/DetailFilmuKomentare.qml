import bb.cascades 1.2
import com.devpda.items 1.2
import com.devpda.tools 1.2


import "../components"
import "../items"

Container {
    id: root
    layout: DockLayout {}
    
    property MovieItem movie: null
    onMovieChanged: {
        for (var item in movie.comments){
            //console.debug("NAME: ".concat(movie.movie_peoples[item].name))
            commentsModel.insert(movie.comments[item])
        }
        if (commentsModel.size()){
            commentsList.visible = true
        }else{
            noComments.visible = true
        }
    }
    
    
    Label {
        id: noComments
        visible: false
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        text: qsTr("No comments") + Retranslate.onLocaleOrLanguageChanged
        textStyle{
            base: SystemDefaults.TextStyles.BigText
            color: Color.Gray
        }
    }
    
    
    ListView {
        id: commentsList
        scrollRole: ScrollRole.Main
        visible: false
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: commentsModel
            grouping: ItemGrouping.ByFullValue
            sortingKeys: ["date", "user", "rating"]
            sortedAscending: true
        }
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemComments{}
            }
        ]
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        
        }
    }
}

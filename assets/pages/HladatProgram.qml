import bb.cascades 1.2

import "../components"
import "../items"

MyPage {
    
    
    function request(response){
        var json = JSON.parse(response)
        model.insertList(json.data)
        App.loading = false
        if (!model.size()){
            toast.body = qsTr("Not found") + Retranslate.onLocaleOrLanguageChanged
            toast.show()
        }
    }
    
    header: TitleHeader {
        id: titleHeader
        titleText: qsTr("Search program") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    loadingText: qsTr("Searching...") + Retranslate.onLocaleOrLanguageChanged
    onActiveChanged: {
        App.hladatProgramDone.connect(request)
        if (active){
            searchField.requestFocus()
        }
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: movieDetail
            source: "DetailFilmu.qml"
        }
    ]
    
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        TextField {
            id: searchField
            bottomMargin: 0
            hintText: titleHeader.titleText
            inputMode: TextFieldInputMode.Text
            autoFit: TextAutoFit.Default
            input.masking: TextInputMasking.Masked
            input.submitKey: SubmitKey.Search
            maximumLength: 40
            onTextChanging: {
                if (!text.length) model.clear()
            }
            backgroundVisible: true
            input.onSubmitted: {
                if (text.length){
                    App.loading = true
                    Api.hladatProgram(text.trim(), true, Settings.searchLimitProgram)
                }
            }
        } // End of SearchField
        
        ListView {
            
            function addNotify(indexPath){
                var item = model.data(indexPath)
                // {"title": Nazov filmu, "body": Popis filmu, "station": Nazov stanice, "guid": Unikatne ID}
                var data = {"title": item.title, "body": item.caption, "station": item.tv_name, "guid": item.id}
                Calendar.addEvent(item.alarm, item.time, data)
            }
            
            opacity: App.loading ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            dataModel: GroupDataModel {
                id: model
                grouping: ItemGrouping.None
                sortingKeys: ["alarm"]
                sortedAscending: true
            }
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemHladatProgram {}
                }
            ]
            onTriggered: {
                var item = dataModel.data(indexPath)
                var page;
                if (item && item.film != 0){
                    var page = movieDetail.createObject()
                    page.title = item.title
                    page.filmId = item.film
                    activePane.push(page)
                }
            }
        }
    }

}
import bb.cascades 1.2
import com.devpda.items 1.2
import com.devpda.tools 1.2

import "../components"
import "../items"
/*FILM OBJECT
 {
 "id": "96166",
 "title": "Expendables: Postradatelní 3",
 "orig_title": "The Expendables 3",
 "image": "996bc61794a3d6505cc7932a7a1d.jpg",
 "year": "2014",
 "rating": "0",
 "job": [
 {
 "title": "hraje",
 "nick": "Max Drummer"
 }
 ],
 "jobfdb": "hraje ... Max Drummer"
 }*/

Container {
    id: root
    
    property ActorItem actor: null
    onActorChanged: {
        for (var item in actor.films){
            filmographyModel.insert(actor.films[item])
        }
    }
    
    
    attachedObjects: [
        ComponentDefinition {
            id: movieDetail
            source: "DetailFilmu.qml"
        }
    ]
    
    ListView {
        id: filmographyList
        scrollRole: ScrollRole.Main
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: GroupDataModel {
            id: filmographyModel
            grouping: ItemGrouping.ByFullValue
            sortingKeys: ["year"]
            sortedAscending: false
        }
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemFilmography{}
            }
        ]
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        }
        
        onTriggered: {
            var item = dataModel.data(indexPath)
            if (item){
                var page = movieDetail.createObject()
                page.title = item.title
                page.filmId = item.id
                activePane.push(page)
            }
        }
    }
}

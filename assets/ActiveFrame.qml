import bb.cascades 1.2
import com.devpda.tools 1.2

Container {
    objectName: "mainContainer"
    layout: DockLayout {
    }
    RemoteImage {
        objectName: "image"
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
    }
    Label {
        objectName: "label"
        text: "AF is refreshed by BB10 every 30sec"
        multiline: true
        verticalAlignment: VerticalAlignment.Top
        horizontalAlignment: HorizontalAlignment.Left
        textStyle.fontSize: FontSize.XSmall
    }
    Label {
        objectName: "label2"
        text: "0"
        multiline: true
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Bottom
        textStyle.fontSize: FontSize.XLarge
    }
}

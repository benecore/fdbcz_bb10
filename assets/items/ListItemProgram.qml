import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"

ListItemBase {
    id: root
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize*2
        rightPadding: root.ListItem.view.multiSelectHandler.active ? highlightFrameSize*16 : highlightFrameSize*2
        topPadding: highlightFrameSize*2
        bottomPadding: highlightFrameSize*2
        

        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            preferredHeight: Qt.SizeHelper.nType ? 100 : 130
            preferredWidth: Qt.SizeHelper.nType ? 100 : 130
            scalingMethod: ScalingMethod.AspectFit
            url: "http://img.fdb.cz/tv56/".concat(ListItemData.logo).replace(".png", "@2x.png")
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
            
        }
        
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Center
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            
            Container {
                objectName: "titleContainer"
                horizontalAlignment: HorizontalAlignment.Fill
                MyLabel {
                    role: "title"
                    text: ListItemData.name
                    multiline: true
                    autoSize.maxLineCount: 1
                    bottomMargin: 0
                    topMargin: 0
                }
            }
        }
        
        CheckBox {
            touchPropagationMode: TouchPropagationMode.None
            verticalAlignment: VerticalAlignment.Center
            checked: ListItemData.favorite
        }
    } // End of LeftToRight container
}

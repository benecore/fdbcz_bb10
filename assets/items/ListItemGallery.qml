import bb.cascades 1.2
import com.devpda.tools 1.2


ListItemBase {
    id: root
    
    highlight: false
    
    leftPadding: 10
    rightPadding: 10
    topPadding: 10
    bottomPadding: 10
    
    
    Container {
        Container {
            layout: DockLayout {}
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            ActivityIndicator {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                visible: (remoteImage.loading)
                running: visible
                preferredHeight: 250
                preferredWidth: 250
            }
            
            
            RemoteImage {
                id: remoteImage
                //horizontalAlignment: HorizontalAlignment.Center
                //verticalAlignment: VerticalAlignment.Center
                preferredHeight: 400
                preferredWidth: Qt.SizeHelper.maxWidth
                scalingMethod: ScalingMethod.AspectFit
                url: "http://img.fdb.cz/galerie/".concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                cache: true
                bottomMargin: 0
            }
        }
        
        Label {
            visible: ListItemData.herci
            topMargin: 0
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom
            text: ListItemData.herci
            multiline: true
            textStyle.textAlign: TextAlign.Center
            textStyle{
                base: SystemDefaults.TextStyles.SubtitleText
                color: Color.Gray
            }
        }  
    
    }
}
import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"

ListItemBase {
    id: root
    
    preferredHeight: Qt.SizeHelper.nType ? 120 : 160
    preferredWidth: Qt.SizeHelper.maxWidth
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/person-placeholder.png"
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            preferredHeight: Qt.SizeHelper.nType ? 114 : 154
            preferredWidth: Qt.SizeHelper.nType ? 70 : 110
            scalingMethod: ScalingMethod.AspectFill
            loadEffect: ImageViewLoadEffect.FadeZoom
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            verticalAlignment: VerticalAlignment.Center
            MyLabel {
                role: "title"
                text: ListItemData.name.concat(" ").concat(ListItemData.lastname)
                bottomMargin: 0
            }
            
            MyLabel {
                visible: ListItemData.nick
                role: "subtitle"
                color: Color.create("#".concat(Qt.Settings.activeColor))
                topMargin: 0
                textStyle.fontWeight: FontWeight.Bold
                text: ListItemData.nick
            }
        }
    }
}
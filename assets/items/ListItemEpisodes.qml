import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"

/*{
"id": "84306",
"serie": "1",
"dil": "1",
"title": "Rozchod",
"orig_title": "Pilot",
"image": "edcf265cdc2927f1527cb1850c9.jpg",
"year": "2003",
"rating": "633"
}*/

ListItemBase {
    id: root
    
    preferredHeight: Qt.SizeHelper.nType ? 140 : 180
    preferredWidth: Qt.SizeHelper.maxWidth
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/movie-placeholder.png"
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            preferredHeight: Qt.SizeHelper.nType ? 114 : 154
            preferredWidth: Qt.SizeHelper.nType ? 70 : 110
            scalingMethod: ScalingMethod.AspectFit
            loadEffect: ImageViewLoadEffect.FadeZoom
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                MyLabel {
                    role: "title"
                    text: ListItemData.title
                    multiline: false
                    bottomMargin: 0
                    topMargin: 0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                FilmRating {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    size: "small"
                    rating: ListItemData.rating
                }
            } // End of Title/Rating Container
            
            MyLabel {
                visible: ListItemData.orig_title
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.orig_title
            }
            
            MyLabel {
                visible: ListItemData.serie
                role: "subtitle"
                color: Color.create("#".concat(Qt.Settings.activeColor))
                topMargin: 0
                bottomMargin: 0
                textStyle.fontWeight: FontWeight.Bold
                text: qsTr("Series %1").arg(ListItemData.serie)
            }
            
            MyLabel {
                visible: ListItemData.year
                role: "subtitle"
                color: Color.create("#".concat(Qt.Settings.activeColor))
                topMargin: 0
                bottomMargin: 0
                textStyle.fontWeight: FontWeight.Bold
                text: qsTr("Episode %1").arg(ListItemData.dil)
            }
        }
        
    } // end of root container
}
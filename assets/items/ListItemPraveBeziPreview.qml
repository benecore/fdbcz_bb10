import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"


Container {
    
    property alias titleText: titleLabel.text
    property alias subtitle1: subtitleLabel1.text
    property alias subtitle2: subtitleLabel2.text
    
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    RemoteImage {
        verticalAlignment: VerticalAlignment.Center
        preferredHeight: Qt.SizeHelper.nType ? 90 : 130
        preferredWidth: Qt.SizeHelper.nType ? 90 : 130
        url: "http://img.fdb.cz/tv56/3sat@2x.png"
        layoutProperties: StackLayoutProperties {
            spaceQuota: -1
        }
        cache: true
    }
    
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Center
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }
        
        Container {
            objectName: "titleContainer"
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                id: titleLabel
                role: "title"
                multiline: true
                autoSize.maxLineCount: 1
                bottomMargin: 0
                topMargin: 0
            }
        }
        
        
        Container {
            objectName: "progressContainer"
            visible: ListItemData.title
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                role: "subtitle"
                horizontalAlignment: HorizontalAlignment.Left
                text: "00:00"
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
            
            ProgressIndicator {
                verticalAlignment: VerticalAlignment.Center
                value: 43
                toValue: 100.0
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            
            
            MyLabel {
                role: "subtitle"
                horizontalAlignment: HorizontalAlignment.Right
                text: "00:00"
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
        } // End of Progress indicator
        
        Container {
            objectName: "programOneContainer"
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            topMargin: 0
            bottomMargin: 0
            MyLabel {
                role: "subtitle"
                textStyle.color: Color.Gray
                text: "00:00"
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
            
            MyLabel {
                id: subtitleLabel1
                role: "subtitle"
                textStyle.color: Color.Gray
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
        } // End of First following program
        
        Container {
            objectName: "programSecondContainer"
            //visible: false
            opacity: 0.8
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            topMargin: 0
            bottomMargin: 0
            MyLabel {
                role: "subtitle"
                textStyle.color: Color.Gray
                text: "00:00"
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
            
            MyLabel {
                id: subtitleLabel2
                role: "subtitle"
                textStyle.color: Color.Gray
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
        } // End of Second following program
    } // End of StackLayout TopBottom container
} // End of LeftToRight container

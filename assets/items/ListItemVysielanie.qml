import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    showNext: ListItemData.film
    
    contextActions: [
        ActionSet {
            title: ListItemData.title
            subtitle: ListItemData.name
            
            actions: [
                CalendarAction {
                    onClicked: {
                        root.ListItem.view.addNotify(root.ListItem.indexPath)
                    }
                }
            ]
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            preferredWidth: Qt.SizeHelper.nType ? 110 : 160
            preferredHeight: Qt.SizeHelper.nType ? 160 : 230
            scalingMethod: ScalingMethod.AspectFill
            defaultImage: "asset:///images/movie-placeholder.png"
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        
        Container{
            verticalAlignment: VerticalAlignment.Top
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                MyLabel {
                    role: "title"
                    horizontalAlignment: HorizontalAlignment.Fill
                    text: ListItemData.title
                    multiline: true
                    autoSize.maxLineCount: 2
                    topMargin: 0
                    bottomMargin: 0
                }
            } // End of title container
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    MyLabel {
                        text: ListItemData.starttime
                        role: "highlight"
                        textStyle.fontWeight: FontWeight.Bold
                        color: Color.create("#".concat(Qt.Settings.activeColor))
                        topMargin: 0
                        bottomMargin: 0
                    }
                    
                    ProgramType {
                        visible: ListItemData.type != 0 && ListItemData.type != 6
                        height: Qt.SizeHelper.nType ? 15 : 25
                        type: ListItemData.type
                    }
                
                }
                
                MyLabel {
                    text: ListItemData.caption ? ListItemData.caption : qsTr("No description available") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    autoSize {
                        maxLineCount: 4
                    }
                    textStyle{
                        fontSize: FontSize.PointValue
                        fontSizeValue: Qt.Settings.fontSizeSubtitle+1
                        fontFamily: Qt.Settings.fontFamilySubtitle
                        color: Color.Gray
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    topMargin: 0
                    leftMargin: 2
                }
            
            }
        }
    }
}
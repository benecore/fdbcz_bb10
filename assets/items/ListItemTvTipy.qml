import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    contextActions: [
        ActionSet {
            title: ListItemData.title
            subtitle: ListItemData.name
            
            actions: [
                CalendarAction {
                    onClicked: {
                        root.ListItem.view.addNotify(root.ListItem.indexPath)
                    }
                },
                BrowserAction {
                    onClicked: {
                        if (ListItemData.film.id != 0){
                            Qt.Helper.openInBrowser(ListItemData.film.id)
                        }
                    }
                }
            ]
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/movie-placeholder.png"
            preferredWidth: Qt.SizeHelper.nType ? 120 : 160
            preferredHeight: Qt.SizeHelper.nType ? 170 : 230
            scalingMethod: ScalingMethod.AspectFill
            url: Qt.Settings.imageUrl.concat(ListItemData.film.image[0]).concat("/").concat(ListItemData.film.image)
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                MyLabel {
                    role: "title"
                    text: ListItemData.film.title
                    multiline: false
                    bottomMargin: 0
                    topMargin: 0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                FilmRating {
                    size: Qt.SizeHelper.nType ? "medium" : "big"
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    rating: ListItemData.film.rating
                }
            } // End of Title/Rating Container
            
            MyLabel {
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.film.zanry
            }
            
            MyLabel {
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.film.zeme
            }
            MyLabel {
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.film.year.concat(" | ").concat(ListItemData.film.length).concat(" min")
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                RemoteImage {
                    preferredHeight: Qt.SizeHelper.nType ? 70 : 90
                    preferredWidth: Qt.SizeHelper.nType ? 70 : 90
                    url: "http://img.fdb.cz/tv56/".concat(ListItemData.logo).replace(".png", "@2x.png")
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    cache: true
                }
                
                MyLabel {
                    role: "highlight"
                    color: Color.create("#".concat(Qt.Settings.activeColor))
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    text: ListItemData.time.split(" - ")[0]
                    textStyle{
                        fontWeight: FontWeight.Bold
                    }
                }
            }
        } // End of Root TopBottom container
    }
}

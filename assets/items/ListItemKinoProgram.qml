import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    showNext: ListItemData.id
    
    contextActions: [
        ActionSet {
            title: ListItemData.film
            subtitle: ListItemData.name
            
            actions: [
                CalendarAction {
                    onClicked: {
                        root.ListItem.view.addNotify(root.ListItem.indexPath)
                    }
                }
            ]
        }
    ]
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/movie-placeholder.png"
            preferredWidth: Qt.SizeHelper.nType ? 120 : 160
            preferredHeight: Qt.SizeHelper.nType ? 170 : 230
            scalingMethod: ScalingMethod.AspectFill
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                MyLabel {
                    role: "title"
                    text: ListItemData.film
                    multiline: false
                    bottomMargin: 0
                    topMargin: 0
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                
                FilmRating {
                    size: Qt.SizeHelper.nType ? "medium" : "big"
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    rating: ListItemData.rating
                }
            } // End of Title/Rating Container
            
            MyLabel {
                visible: ListItemData.zanry
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.zanry
            }
            
            MyLabel {
                visible: ListItemData.zeme
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.zeme
            }
            MyLabel {
                visible: ListItemData.year
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.year.concat(" ").concat(ListItemData.length ? "|" + ListItemData.length + " min" : "")
            }
            
            
            MyLabel {
                role: "subtitle"
                multiline: true
                autoSize.maxLineCount: 2
                color: Color.create("#".concat(Qt.Settings.activeColor))
                verticalAlignment: VerticalAlignment.Center
                text: ListItemData.time
                textStyle{
                    fontWeight: FontWeight.Bold
                }
            }
        } // End of Root TopBottom container
    }
}

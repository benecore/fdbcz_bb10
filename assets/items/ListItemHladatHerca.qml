import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"
import "../actions"

ListItemBase {
    id: root
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        
        RemoteImage {
            verticalAlignment: VerticalAlignment.Center
            defaultImage: "asset:///images/person-placeholder.png"
            preferredHeight: Qt.SizeHelper.nType ? 114 : 154
            preferredWidth: Qt.SizeHelper.nType ? 70 : 110
            scalingMethod: ScalingMethod.AspectFill
            url: Qt.Settings.imageUrl.concat(ListItemData.image[0]).concat("/").concat(ListItemData.image)
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            cache: true
        }
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            
            MyLabel {
                role: "title"
                text: ListItemData.name.concat(" ").concat(ListItemData.lastname)
                multiline: false
                bottomMargin: 0
                topMargin: 0
            }
            MyLabel {
                visible: ListItemData.nick
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.nick.trim()
            }
            
            MyLabel {
                visible: ListItemData.profession
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.profession.trim()
            }
            MyLabel {
                visible: ListItemData.birthday 
                role: "subtitle"
                color: Color.Gray
                topMargin: 0
                bottomMargin: 0
                text: ListItemData.birthday.concat(" ").concat(ListItemData.age ? "(" + ListItemData.age + ")" : "")
            }
        }
    }
}
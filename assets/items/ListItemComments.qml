import bb.cascades 1.2
import com.devpda.tools 1.2

import "../components"

ListItemBase {
    id: root
    
    highlight: false
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        leftPadding: 5
        rightPadding: 5
        
        MyRating {
            horizontalAlignment: HorizontalAlignment.Center
            rating: parseInt(ListItemData.rating)
            bottomMargin: 0
            topMargin: 0
        }
        
        MyLabel {
            role: "highlight"
            textStyle.fontWeight: FontWeight.Bold
            color: Color.create("#".concat(Qt.Settings.activeColor))
            text: ListItemData.user
            bottomMargin: 0
            topMargin: 0
        }
        
        Label {
            //role: "title"
            text: ListItemData.text
            multiline: true
            topMargin: 0
            textStyle{
                fontStyle: FontStyle.Italic
                base: Qt.SizeHelper.nType ? SystemDefaults.TextStyles.SubtitleText : SystemDefaults.TextStyles.PrimaryText
            }
        }
    }
}
import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    
    title: qsTr("Search") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/search.png"
    onTriggered: {
        root.clicked()
    }
}
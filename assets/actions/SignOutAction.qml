import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    
    title: qsTr("Sign Out") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/signout.png"
    onTriggered: {
        root.clicked()
    }
}
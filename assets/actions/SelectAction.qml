import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    property bool selectAll: true
    
    signal clicked()
    
    //title: selectAll ? qsTr("Vybrať všetky") + Retranslate.onLocaleOrLanguageChanged : qsTr("Vybrať ") + Retranslate.onLocaleOrLanguageChanged
    imageSource: selectAll ? "asset:///images/actions/select_all.png" : "asset:///images/actions/select_none.png"
    onTriggered: {
        root.clicked()
    }
}
import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    
    signal clicked()
    
    title: qsTr("Show on Map") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/show_map.png"
    onTriggered: {
        root.clicked()
    }
}
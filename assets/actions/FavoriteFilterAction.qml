import bb.cascades 1.2

ActionItem {
    id: root
    
    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    
    title: qsTr("Filter favorites") + Retranslate.onLocaleOrLanguageChanged
    imageSource: Settings.filterTipy ? "asset:///images/actions/favorite.png" : "asset:///images/actions/favorite_off.png"
    onTriggered: {
        root.clicked()
    }
}
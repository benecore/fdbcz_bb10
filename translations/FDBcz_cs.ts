<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>About</name>
    <message>
        <location filename="../assets/pages/About.qml" line="33"/>
        <source>Thanks for support</source>
        <translation>Děkuji za podporu</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="96"/>
        <source>Version: %1</source>
        <translation>Verze: %1</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="108"/>
        <source>about_app_description</source>
        <translation>Plnohodnotná aplikace pro filmové fajnšmekry. Přehledný tv program, tv tipy, informace o filmech a osobnostech, kina a to vše na jednom místě ve vašem smartphone</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="124"/>
        <source>If you like the app support its further development.</source>
        <translation>Pokud se vám aplikace líbí podpořte její další vývoj.</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="164"/>
        <source>Developer</source>
        <translation>Vývojář</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="201"/>
        <source>In case of any questions or ideas, feel free to contact me</source>
        <translation>V případě jakýchkoliv dotazů nebo nápadů mě neváhejte kontaktovat</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="227"/>
        <source>Data povides</source>
        <translation>Data zajišťuje</translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="279"/>
        <source>Thanks</source>
        <translation>Poděkování</translation>
    </message>
</context>
<context>
    <name>AddAction</name>
    <message>
        <location filename="../assets/actions/AddAction.qml" line="11"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
</context>
<context>
    <name>BackAction</name>
    <message>
        <location filename="../assets/actions/BackAction.qml" line="11"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
</context>
<context>
    <name>BrowserAction</name>
    <message>
        <location filename="../assets/actions/BrowserAction.qml" line="11"/>
        <source>Open in browser</source>
        <translation>Otevřít v prohlížeči</translation>
    </message>
</context>
<context>
    <name>CalendarAction</name>
    <message>
        <location filename="../assets/actions/CalendarAction.qml" line="11"/>
        <source>Add notify</source>
        <translation>Přidat upozornění</translation>
    </message>
</context>
<context>
    <name>CalendarHandler</name>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="28"/>
        <source>Warn before program</source>
        <translation>Upozornit před programem</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="30"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="34"/>
        <source>0 min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="34"/>
        <source>5 min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="34"/>
        <source>10 min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="34"/>
        <source>20 min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="34"/>
        <source>30 min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="34"/>
        <source>1 hour</source>
        <translation>1 hodinu</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="35"/>
        <source>1 day</source>
        <translation>1 den</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="35"/>
        <source>2 days</source>
        <translation>2 dny</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="35"/>
        <source>3 days</source>
        <translation>3 dny</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="35"/>
        <source>4 days</source>
        <translation>4 dny</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="35"/>
        <source>5 days</source>
        <translation>5 dnů</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="73"/>
        <location filename="../src/custom/calendarhandler.cpp" line="101"/>
        <source>Program is no longer possible to warn</source>
        <translation>Na program již nelze upozornit</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="225"/>
        <source>Is already on the calendar</source>
        <translation>Upozornění se již nachází v kalendáři</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="265"/>
        <source>Notify was added</source>
        <translation>Upozornění bylo přidáno</translation>
    </message>
    <message>
        <location filename="../src/custom/calendarhandler.cpp" line="267"/>
        <source>Unable to add notify</source>
        <translation>Nelze přidat upozornění</translation>
    </message>
</context>
<context>
    <name>ChcemVidietAction</name>
    <message>
        <source>I want to see</source>
        <translation type="obsolete">Chci vidět</translation>
    </message>
</context>
<context>
    <name>ClearAction</name>
    <message>
        <location filename="../assets/actions/ClearAction.qml" line="11"/>
        <source>Clear all</source>
        <translation>Odstranit všechny</translation>
    </message>
</context>
<context>
    <name>CustomDialog</name>
    <message>
        <location filename="../assets/components/CustomDialog.qml" line="60"/>
        <source>Rate movie</source>
        <translation>Hodnotit film</translation>
    </message>
    <message>
        <location filename="../assets/components/CustomDialog.qml" line="111"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../assets/components/CustomDialog.qml" line="118"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
</context>
<context>
    <name>DetailFilmu</name>
    <message>
        <location filename="../assets/pages/DetailFilmu.qml" line="55"/>
        <source>Movie detail</source>
        <translation>Detail filmu</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmu.qml" line="174"/>
        <source>Episodes</source>
        <translation>Epizody</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmu.qml" line="190"/>
        <source>Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmu.qml" line="193"/>
        <source>Cast</source>
        <translation>Obsazení</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmu.qml" line="196"/>
        <source>Comments</source>
        <translation>Komentáře</translation>
    </message>
</context>
<context>
    <name>DetailFilmuEpisodes</name>
    <message>
        <location filename="../assets/pages/DetailFilmuEpisodes.qml" line="42"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
</context>
<context>
    <name>DetailFilmuInfo</name>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="136"/>
        <source>Basic information</source>
        <translation>Základní informace</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="172"/>
        <source>Original name:</source>
        <translation>Originální název:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="192"/>
        <source>Year:</source>
        <translation>Rok:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="208"/>
        <source>State:</source>
        <translation>Stát:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="224"/>
        <source>Genre:</source>
        <translation>Žánr:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="53"/>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="267"/>
        <source>Rating</source>
        <translation>Hodnocení</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="276"/>
        <source>I saw</source>
        <translation>Viděl jsem</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="285"/>
        <source>I want to see</source>
        <translation>Chci vidět</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="299"/>
        <source>Broadcast</source>
        <translation>Vysílání</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="387"/>
        <source>Detailed information</source>
        <translation>Podrobné informace</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="396"/>
        <source>Description:</source>
        <translation>Popis:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailFilmuInfo.qml" line="403"/>
        <source>Description not available</source>
        <translation>Popis není k dispozici</translation>
    </message>
</context>
<context>
    <name>DetailFilmuKomentare</name>
    <message>
        <location filename="../assets/pages/DetailFilmuKomentare.qml" line="32"/>
        <source>No comments</source>
        <translation>Žádné komentáře</translation>
    </message>
</context>
<context>
    <name>DetailHerca</name>
    <message>
        <location filename="../assets/pages/DetailHerca.qml" line="41"/>
        <source>Actor detail</source>
        <translation>Detail herce</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHerca.qml" line="99"/>
        <source>Info</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHerca.qml" line="102"/>
        <source>Filmography</source>
        <translation>Filmografie</translation>
    </message>
</context>
<context>
    <name>DetailHercaInfo</name>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="64"/>
        <source>Basic information</source>
        <translation>Základní informace</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="101"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="121"/>
        <source>Birth:</source>
        <translation>Narození:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="137"/>
        <source>Death:</source>
        <translation>Smrt:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="153"/>
        <source>Nationality:</source>
        <translation>Národnost:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="170"/>
        <source>Detailed information</source>
        <translation>Podrobné informace</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="181"/>
        <source>Profession:</source>
        <translation>Profese:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="198"/>
        <source>Bio:</source>
        <translation>Životopis:</translation>
    </message>
    <message>
        <location filename="../assets/pages/DetailHercaInfo.qml" line="205"/>
        <source>Bio not available</source>
        <translation>Životopis není k dispozici</translation>
    </message>
</context>
<context>
    <name>FavoriteAction</name>
    <message>
        <location filename="../assets/actions/FavoriteAction.qml" line="20"/>
        <source>Add to favorite</source>
        <translation>Přidat k oblíbeným</translation>
    </message>
    <message>
        <location filename="../assets/actions/FavoriteAction.qml" line="20"/>
        <source>Remove from favorite</source>
        <translation>Odstranit z oblíbených</translation>
    </message>
</context>
<context>
    <name>FavoriteFilterAction</name>
    <message>
        <location filename="../assets/actions/FavoriteFilterAction.qml" line="11"/>
        <source>Filter favorites</source>
        <translation>Filtrovat tipy</translation>
    </message>
</context>
<context>
    <name>Galeria</name>
    <message>
        <location filename="../assets/pages/Galeria.qml" line="14"/>
        <source>Gallery</source>
        <translation>Galerie</translation>
    </message>
</context>
<context>
    <name>GalleryAction</name>
    <message>
        <location filename="../assets/actions/GalleryAction.qml" line="10"/>
        <source>Gallery</source>
        <translation>Galerie</translation>
    </message>
</context>
<context>
    <name>Hladat</name>
    <message>
        <location filename="../assets/tabs/Hladat.qml" line="23"/>
        <source>Not found</source>
        <translation>Nenalezeno</translation>
    </message>
    <message>
        <location filename="../assets/tabs/Hladat.qml" line="34"/>
        <source>Searching...</source>
        <translation>Vyhledávání...</translation>
    </message>
    <message>
        <location filename="../assets/tabs/Hladat.qml" line="89"/>
        <source>Movie</source>
        <translation>Film</translation>
    </message>
    <message>
        <location filename="../assets/tabs/Hladat.qml" line="93"/>
        <source>Person</source>
        <translation>Osobnost</translation>
    </message>
</context>
<context>
    <name>HladatProgram</name>
    <message>
        <location filename="../assets/pages/HladatProgram.qml" line="14"/>
        <source>Not found</source>
        <translation>Nenalezeno</translation>
    </message>
    <message>
        <location filename="../assets/pages/HladatProgram.qml" line="21"/>
        <source>Search program</source>
        <translation>Hledat program</translation>
    </message>
    <message>
        <location filename="../assets/pages/HladatProgram.qml" line="24"/>
        <source>Searching...</source>
        <translation>Vyhledávání...</translation>
    </message>
</context>
<context>
    <name>HrajuKina</name>
    <message>
        <location filename="../assets/tabs/HrajuKina.qml" line="53"/>
        <source>Add notify</source>
        <translation>Přidat upozornění</translation>
    </message>
    <message>
        <location filename="../assets/tabs/HrajuKina.qml" line="55"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../assets/tabs/HrajuKina.qml" line="56"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../assets/tabs/HrajuKina.qml" line="90"/>
        <source>Today</source>
        <translation>Dnes</translation>
    </message>
    <message>
        <location filename="../assets/tabs/HrajuKina.qml" line="93"/>
        <source>Tomorrow</source>
        <translation>Zítra</translation>
    </message>
    <message>
        <location filename="../assets/tabs/HrajuKina.qml" line="96"/>
        <source>Day after tomorrow</source>
        <translation>Pozítří</translation>
    </message>
</context>
<context>
    <name>KinoPremiery</name>
    <message>
        <source>Today</source>
        <translation type="obsolete">Dnes</translation>
    </message>
    <message>
        <source>Tomorrow</source>
        <translation type="obsolete">Zítra</translation>
    </message>
</context>
<context>
    <name>KinoPridat</name>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="16"/>
        <source>Karlovarsky</source>
        <translation>Karlovarský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="17"/>
        <source>Ustecky</source>
        <translation>Ústecký</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="18"/>
        <source>Liberecky</source>
        <translation>Liberecký</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="19"/>
        <source>Kralovehradecky</source>
        <translation>Královéhradecký</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="20"/>
        <source>Pardubicky</source>
        <translation>Pardubický</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="21"/>
        <source>Olomoucky</source>
        <translation>Olomoucký</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="22"/>
        <source>Moravskoslezsky</source>
        <translation>Moravskoslezský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="23"/>
        <source>Zlinsky</source>
        <translation>Zlínský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="24"/>
        <source>Jihomoravsky</source>
        <translation>Jihomoravský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="25"/>
        <source>Vysocina</source>
        <translation>Vysočina</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="26"/>
        <source>Jihocesky</source>
        <translation>Jihočeský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="27"/>
        <source>Plzensky</source>
        <translation>Plzeňský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="28"/>
        <source>Hlavni mesto Praha</source>
        <translation>Hlavní město Praha</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="29"/>
        <source>Stredocesky</source>
        <translation>Středočeský</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="74"/>
        <source>Add cinema</source>
        <translation>Přidat kino</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="98"/>
        <source>Cities</source>
        <translation>Města</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoPridat.qml" line="116"/>
        <source>Region</source>
        <translation>Kraj</translation>
    </message>
</context>
<context>
    <name>KinoProgram</name>
    <message>
        <location filename="../assets/pages/KinoProgram.qml" line="87"/>
        <source>Add notify</source>
        <translation>Přidat upozornění</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoProgram.qml" line="89"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../assets/pages/KinoProgram.qml" line="90"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>ListItemBeziOd</name>
    <message>
        <location filename="../assets/items/ListItemBeziOd.qml" line="104"/>
        <location filename="../assets/items/ListItemBeziOd.qml" line="213"/>
        <source>Not broadcast</source>
        <translation>Nevysílá</translation>
    </message>
</context>
<context>
    <name>ListItemEpisodes</name>
    <message>
        <location filename="../assets/items/ListItemEpisodes.qml" line="92"/>
        <source>Series %1</source>
        <translation>Série %1</translation>
    </message>
    <message>
        <location filename="../assets/items/ListItemEpisodes.qml" line="102"/>
        <source>Episode %1</source>
        <translation>Epizoda %1</translation>
    </message>
</context>
<context>
    <name>ListItemHladatProgram</name>
    <message>
        <location filename="../assets/items/ListItemHladatProgram.qml" line="110"/>
        <source>No description available</source>
        <translation>Popis není k dispozici</translation>
    </message>
</context>
<context>
    <name>ListItemKinoHraju</name>
    <message>
        <location filename="../assets/items/ListItemKinoHraju.qml" line="29"/>
        <source>Cinema web</source>
        <translation>Web kina</translation>
    </message>
</context>
<context>
    <name>ListItemOblubeneKina</name>
    <message>
        <location filename="../assets/items/ListItemOblubeneKina.qml" line="28"/>
        <source>Remove all</source>
        <translation>Odstranit všechny</translation>
    </message>
    <message>
        <location filename="../assets/items/ListItemOblubeneKina.qml" line="35"/>
        <source>Remove all cinemas?</source>
        <translation>Odstranit všechny kina?</translation>
    </message>
    <message>
        <location filename="../assets/items/ListItemOblubeneKina.qml" line="36"/>
        <source>Are you sure you want to remove all favorite cinemas?</source>
        <translation>Jste si jistý, že chcete odstranit všechny oblíbená kina?</translation>
    </message>
    <message>
        <location filename="../assets/items/ListItemOblubeneKina.qml" line="38"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
</context>
<context>
    <name>ListItemPraveBezi</name>
    <message>
        <location filename="../assets/items/ListItemPraveBezi.qml" line="85"/>
        <source>Not broadcast</source>
        <translation>Nevysílá</translation>
    </message>
</context>
<context>
    <name>ListItemVysielanie</name>
    <message>
        <location filename="../assets/items/ListItemVysielanie.qml" line="100"/>
        <source>No description available</source>
        <translation>Popis není k dispozici</translation>
    </message>
</context>
<context>
    <name>LoadingIndicator</name>
    <message>
        <location filename="../assets/components/LoadingIndicator.qml" line="22"/>
        <source>Loading...</source>
        <translation>Načítání...</translation>
    </message>
</context>
<context>
    <name>MapBubble</name>
    <message>
        <location filename="../assets/components/MapBubble.qml" line="67"/>
        <source>Hey, Lets go!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MojeFDB</name>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="27"/>
        <source>You must fill email to login</source>
        <translation>Musíte vyplnit e-mail pro přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="30"/>
        <source>You must fill password to login</source>
        <translation>Musíte vyplnit heslo pro přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="53"/>
        <source>Login success</source>
        <translation>Přihlášení úspěšné</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="61"/>
        <source>Login failed. Check your email adress</source>
        <translation>Přihlášení se nezdařilo. Zkontrolujte svůj e-mail</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="64"/>
        <source>Login failed. Check your password</source>
        <translation>Přihlášení se nezdařilo. Zkontrolujte vaše heslo</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="74"/>
        <source>Registration success. Your password was generated automatically and sended to your email. You&apos;ve been automatically logged in.</source>
        <translation>Registrace úspěšná. Vaše heslo bylo vygenerováno automaticky a zasláno na Váš e-mail.Byli jste automaticky přihlášeni.</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="75"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="206"/>
        <source>password</source>
        <translation>heslo</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="225"/>
        <source>Remember login details</source>
        <translation>Pamatovat přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="232"/>
        <source>Sign In</source>
        <translation>Přihlášení</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="245"/>
        <source>Don&apos;t you have FDB.cz account?</source>
        <translation>Nemáte ještě účet FDB.cz?</translation>
    </message>
    <message>
        <source>Sign Out</source>
        <translation type="obsolete">Odhlášení</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="264"/>
        <source>I want to see</source>
        <translation>Chci vidět</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="266"/>
        <source>I saw</source>
        <translation>Viděl jsem</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="268"/>
        <source>Rating</source>
        <translation>Hodnotil jsem</translation>
    </message>
    <message>
        <location filename="../assets/tabs/MojeFDB.qml" line="251"/>
        <source>Register</source>
        <translation>Registrovat</translation>
    </message>
</context>
<context>
    <name>MovieItem</name>
    <message>
        <location filename="../src/items/movieitem.cpp" line="24"/>
        <source>Director</source>
        <translation>Režie</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="29"/>
        <source>Scriptwriter</source>
        <translation>Scénář</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="34"/>
        <source>Storie</source>
        <translation>Motiv</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="39"/>
        <source>Music</source>
        <translation>Hudba</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="44"/>
        <source>Camera</source>
        <translation>Kamera</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="49"/>
        <source>Editing</source>
        <translation>Úprava</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="54"/>
        <source>Actor</source>
        <translation>Hrají</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="59"/>
        <source>Performer</source>
        <translation>Účinkují</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="64"/>
        <source>Dubbing</source>
        <translation>Dabing</translation>
    </message>
    <message>
        <location filename="../src/items/movieitem.cpp" line="69"/>
        <source>Voice</source>
        <translation>Hlas</translation>
    </message>
</context>
<context>
    <name>MyPage</name>
    <message>
        <location filename="../assets/components/MyPage.qml" line="23"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../assets/components/MyPage.qml" line="62"/>
        <source>No data</source>
        <translation>Žádná data</translation>
    </message>
</context>
<context>
    <name>NastavenieOblubenych</name>
    <message>
        <location filename="../assets/pages/NastavenieOblubenych.qml" line="49"/>
        <source>Favorite stations</source>
        <translation>Oblíbené stanice</translation>
    </message>
    <message>
        <location filename="../assets/pages/NastavenieOblubenych.qml" line="50"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/pages/NastavenieOblubenych.qml" line="64"/>
        <source>Select more</source>
        <translation>Vybrat více</translation>
    </message>
    <message>
        <location filename="../assets/pages/NastavenieOblubenych.qml" line="140"/>
        <source>selected stations</source>
        <translation>vybrané stanice</translation>
    </message>
    <message>
        <location filename="../assets/pages/NastavenieOblubenych.qml" line="142"/>
        <source>1 selected station</source>
        <translation>1 vybraná stanice</translation>
    </message>
    <message>
        <location filename="../assets/pages/NastavenieOblubenych.qml" line="144"/>
        <source>no selected</source>
        <translation>nevybráno</translation>
    </message>
</context>
<context>
    <name>NavigateAction</name>
    <message>
        <location filename="../assets/actions/NavigateAction.qml" line="10"/>
        <source>Navigate</source>
        <translation>Navigovat</translation>
    </message>
</context>
<context>
    <name>PlayAction</name>
    <message>
        <location filename="../assets/actions/PlayAction.qml" line="10"/>
        <source>Play</source>
        <translation>Přehrát</translation>
    </message>
</context>
<context>
    <name>PraveBezi</name>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="46"/>
        <source>Others</source>
        <translation>Ostatní</translation>
    </message>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="46"/>
        <source>Favorite</source>
        <translation>Oblíbené</translation>
    </message>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="98"/>
        <source>Favorite stations</source>
        <translation>Oblíbené stanice</translation>
    </message>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="203"/>
        <source>selected stations</source>
        <translation>vybrané stanice</translation>
    </message>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="205"/>
        <source>1 selected station</source>
        <translation>1 vybraná stanice</translation>
    </message>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="207"/>
        <source>no selected</source>
        <translation>nevybráno</translation>
    </message>
    <message>
        <location filename="../assets/tabs/PraveBezi.qml" line="282"/>
        <source>Station does not broadcast</source>
        <translation>Stanice nevysílá</translation>
    </message>
</context>
<context>
    <name>ProgramType</name>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="57"/>
        <source>Fun</source>
        <translation>Zábava</translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="59"/>
        <source>Sport</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="61"/>
        <source>Movie</source>
        <translation>Film</translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="63"/>
        <source>Serial</source>
        <translation>Seriál</translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="65"/>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="67"/>
        <source>Music</source>
        <translation>Hudba</translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="69"/>
        <source>Children</source>
        <translation>Děti</translation>
    </message>
    <message>
        <location filename="../assets/components/ProgramType.qml" line="71"/>
        <source>News</source>
        <translation>Zprávy</translation>
    </message>
</context>
<context>
    <name>PullDownRefresh</name>
    <message>
        <location filename="../assets/components/PullDownRefresh.qml" line="39"/>
        <source>Release to refresh...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/components/PullDownRefresh.qml" line="45"/>
        <source>Pull down to refresh...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RefreshAction</name>
    <message>
        <location filename="../assets/actions/RefreshAction.qml" line="12"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
</context>
<context>
    <name>Register</name>
    <message>
        <location filename="../assets/sheet/Register.qml" line="33"/>
        <source>You must fill username to register</source>
        <translation>Musíte vyplnit uživatelské jméno pro registraci</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="36"/>
        <source>You must fill email to register</source>
        <translation>Musíte vyplnit e-mail pro registraci</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="45"/>
        <source>Register an account</source>
        <translation>Registrace účtu</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="87"/>
        <source>What do you get registration</source>
        <translation>Co registrací získáte?</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="92"/>
        <source>register_description</source>
        <translation>• Možnost komentovat a hodnotit filmy přímo z aplikace
• Tvořit si seznam filmů, které chcete vidět, ve kterém pak můžete pohodlně vyhledávat a zjišťovat zda film neběží v některém z Vašich oblíbených kin či v TV.</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="100"/>
        <source>username</source>
        <translation>uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="125"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="133"/>
        <source>Register</source>
        <translation>Registrovat</translation>
    </message>
    <message>
        <location filename="../assets/sheet/Register.qml" line="143"/>
        <source>terms and conditions of FDB.cz</source>
        <translation>Obchodní podmínky FDB.cz</translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="116"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="128"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="134"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="142"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="148"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="156"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="162"/>
        <source>Check your Internet connection and try again.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="169"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="174"/>
        <source>Determining the status. Please wait.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="184"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchAction</name>
    <message>
        <location filename="../assets/actions/SearchAction.qml" line="11"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../assets/pages/Settings.qml" line="57"/>
        <source>General</source>
        <translation>Hlavní</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="57"/>
        <source>Station logo, filtering, images quality etc...</source>
        <translation>Logo stanice, filtrování, kvalita obrázků atd...</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="58"/>
        <source>Appearance</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="58"/>
        <source>Application theme, highlight colors etc..</source>
        <translation>Téma aplikace, barva zvýraznění atd...</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="59"/>
        <source>Calendar</source>
        <translation>Kalendář</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="59"/>
        <source>Calendar, notifications etc...</source>
        <translation>Kalendář, oznámení atd...</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="60"/>
        <source>Searching</source>
        <translation>Vyhledávání</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="60"/>
        <source>Searching limit etc...</source>
        <translation>Limit výsledků atd...</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="61"/>
        <source>Cache</source>
        <translation>Mezipaměť</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="61"/>
        <source>Cache limit, cached images etc...</source>
        <translation>Limit mezipaměti obrázků, uložené obrázky atd...</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="62"/>
        <source>Backup/Restore</source>
        <translation>Zálohování/Obnovení</translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="62"/>
        <source>Backup/Restore favorite stations, cinemas etc...</source>
        <translation>Zálohování/Obnovení oblíbených stanic, kin atd...</translation>
    </message>
</context>
<context>
    <name>SettingsAppearance</name>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="157"/>
        <source>To apply changes restart application</source>
        <translation>Pro aplikování změn restartujte aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="173"/>
        <source>Application theme</source>
        <translation>Téma aplikace</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="183"/>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="186"/>
        <source>Bright</source>
        <translation>Světla</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="191"/>
        <source>Dark</source>
        <translation>Tmavá</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="202"/>
        <source>Font</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="216"/>
        <location filename="../assets/pages/SettingsAppearance.qml" line="329"/>
        <source>Title font</source>
        <translation>Hlavní písmo</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="271"/>
        <location filename="../assets/pages/SettingsAppearance.qml" line="330"/>
        <source>Subtitle font</source>
        <translation>Vedlejší písmo</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="322"/>
        <source>Preview</source>
        <translation>Ukázka</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsAppearance.qml" line="336"/>
        <source>Highlight color</source>
        <translation>Barva zvýraznění</translation>
    </message>
</context>
<context>
    <name>SettingsBackupRestore</name>
    <message>
        <location filename="../assets/pages/SettingsBackupRestore.qml" line="36"/>
        <source>backup_restore_description</source>
        <translation>FDB.cz uloží nastavení vašich oblíbených stanic a kin do souboru (fdbcz.backup). Použijte soubor pro pozdější obnovení po odinstalování aplikace</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsBackupRestore.qml" line="45"/>
        <source>Backup</source>
        <translation>Zálohování</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsBackupRestore.qml" line="56"/>
        <source>FDB.cz backup</source>
        <translation>FDB.cz zálohování</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsBackupRestore.qml" line="61"/>
        <source>Settings was saved!</source>
        <translation>Nastavení uložená!</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsBackupRestore.qml" line="74"/>
        <source>Restore</source>
        <translation>Obnovení</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsBackupRestore.qml" line="86"/>
        <source>Settings was restored!</source>
        <translation>Nastavení obnoveny!</translation>
    </message>
</context>
<context>
    <name>SettingsCache</name>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="64"/>
        <source>Summary</source>
        <translation>Shrnutí</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="90"/>
        <source>count</source>
        <translation>počet položek</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="119"/>
        <source>total size</source>
        <translation>celková velikost</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="126"/>
        <source>Memory limit</source>
        <translation>Limit paměti</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="140"/>
        <source>cache_description</source>
        <translation>Všechny obrázky v aplikaci se ukládají kvůli šetření dat. Na překročení limitu nebudete nijak upozorněn. Nové obrázky se automaticky přestanou ukládat lokálně</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="151"/>
        <source>Limit</source>
        <translation>Aktuální limit</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="175"/>
        <source>Clear</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="182"/>
        <source>Clear cache?</source>
        <translation>Vymazat mezipaměť?</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="183"/>
        <source>Are you sure you want to clear thumbnails cache?</source>
        <translation>Jste si jistý, že chcete vymazat mezipaměť obrázků?</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCache.qml" line="185"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
</context>
<context>
    <name>SettingsCalendar</name>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="12"/>
        <source> day</source>
        <translation>den</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="13"/>
        <source> days</source>
        <translation>dny</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="45"/>
        <source>Notify</source>
        <translation>Upozornění</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="58"/>
        <source>calendar_manualnotify_desc</source>
        <translation>V případě manuálního přidání se vám otevře kalendář s předvyplněnými hodnotami</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="66"/>
        <source>Add manually</source>
        <translation>Přidat ručně</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="75"/>
        <source>Warn program</source>
        <translation>Upozornění na program</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="88"/>
        <source>The single alarm time</source>
        <translation>Jednotný čas upozornění</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="105"/>
        <source>Warn before tv program</source>
        <translation>Upozornit před tv programem</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsCalendar.qml" line="143"/>
        <source>Warn before cinema program</source>
        <translation>Upozornit před kino programem</translation>
    </message>
</context>
<context>
    <name>SettingsGeneral</name>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="29"/>
        <source>Application language</source>
        <translation>Jazyk aplikace</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="40"/>
        <source>Current language</source>
        <translation>Aktuální jazyk</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="43"/>
        <source>Czech</source>
        <translation>Čeština</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="48"/>
        <source>Slovak</source>
        <translation>Slovenčina</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="62"/>
        <source>Tv</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="157"/>
        <source>Show only active stations</source>
        <translation>Aktivní stanice</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="163"/>
        <source>Hides all stations, which currently does not broadcast.</source>
        <translation>Skryje stanice které aktuálně nevysílají</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="198"/>
        <source>Show first program</source>
        <translation>První následující program</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="203"/>
        <source>Show/Hide first following program</source>
        <translation>Zobrazí/Skryje první následující program</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="239"/>
        <source>Show second program</source>
        <translation>Druhý následující program</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="244"/>
        <source>Show/Hide second following program</source>
        <translation>Zobrazí/Skryje druhý následující program</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="265"/>
        <source>Tv Tips</source>
        <translation>Tv tipy</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="283"/>
        <source>Filter</source>
        <translation>Filtrovat</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="289"/>
        <source>Filter tv tips according to favorites stations</source>
        <translation>Filtrování tv tipů podle oblíbených stanic</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="310"/>
        <source>Default view</source>
        <translation>Výchozí zobrazení</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="323"/>
        <source>If you have set favorite stations you can choose which view will be displayed as default after start application</source>
        <translation>V případě, že máte nastavene oblíbené stanice, máte možnost vybrat výchozí zobrazení po startu aplikace</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="329"/>
        <source>Current view</source>
        <translation>Aktuální zobrazení</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="332"/>
        <source>Others</source>
        <translation>Ostatní</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="336"/>
        <source>Favorite</source>
        <translation>Oblíbené</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="348"/>
        <source>Images quality</source>
        <translation>Kvalita obrázků</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="356"/>
        <source>Current quality</source>
        <translation>Aktuální kvalita</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="361"/>
        <source>Low</source>
        <translation>Nízká</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="370"/>
        <source>Medium</source>
        <translation>Střední</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsGeneral.qml" line="379"/>
        <source>High</source>
        <translation>Vysoká</translation>
    </message>
</context>
<context>
    <name>SettingsSearch</name>
    <message>
        <source>Results limit</source>
        <translation type="obsolete">Limit výsledků</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsSearch.qml" line="31"/>
        <source>Searching movie/person</source>
        <translation>Vyhledávání filmu/osobnosti</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsSearch.qml" line="44"/>
        <source>Limit of results which will be displayed in searching. Default limit is 10.</source>
        <translation>Limit výsledků zobrazených ve vyhledávání filmu / herce. Výchozí limit je 10.</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsSearch.qml" line="57"/>
        <location filename="../assets/pages/SettingsSearch.qml" line="114"/>
        <source>Current limit</source>
        <translation>Aktuální limit</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsSearch.qml" line="88"/>
        <source>Searching tv program</source>
        <translation>Vyhledávání tv programu</translation>
    </message>
    <message>
        <location filename="../assets/pages/SettingsSearch.qml" line="101"/>
        <source>Limit of results which will be displayed in searching for program. Default limit is 10.</source>
        <translation>Limit výsledků zobrazených ve vyhledávání tv programu. Výchozí limit je 10.</translation>
    </message>
</context>
<context>
    <name>ShowMapAction</name>
    <message>
        <location filename="../assets/actions/ShowMapAction.qml" line="11"/>
        <source>Show on Map</source>
        <translation>Zobrazit na mapě</translation>
    </message>
</context>
<context>
    <name>SignOutAction</name>
    <message>
        <location filename="../assets/actions/SignOutAction.qml" line="11"/>
        <source>Sign Out</source>
        <translation>Odhlášení</translation>
    </message>
</context>
<context>
    <name>SortingVisual</name>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="14"/>
        <source>Sorting by</source>
        <translation>Třídit podle</translation>
    </message>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="19"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="24"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="29"/>
        <source>Rating count</source>
        <translation>Počet hodnocení</translation>
    </message>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="40"/>
        <source>Sorting</source>
        <translation>Třídit</translation>
    </message>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="44"/>
        <source>Ascending</source>
        <translation>Vzestupně</translation>
    </message>
    <message>
        <location filename="../assets/components/SortingVisual.qml" line="48"/>
        <source>Descending</source>
        <translation>Sestupně</translation>
    </message>
</context>
<context>
    <name>TitleHeader</name>
    <message>
        <location filename="../assets/components/TitleHeader.qml" line="25"/>
        <location filename="../assets/components/TitleHeader.qml" line="37"/>
        <source>Now</source>
        <translation>Nyní</translation>
    </message>
</context>
<context>
    <name>TvTipy</name>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="32"/>
        <source>No tips for your favorite stations</source>
        <translation>Žádné tv tipy pro vaše oblíbené stanice</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="37"/>
        <source>No data</source>
        <translation>Žádná data</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="55"/>
        <source>Filtration: %1</source>
        <translation>Filtrování: %1</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="55"/>
        <source>active</source>
        <translation>aktivní</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="55"/>
        <source>inactive</source>
        <translation>neaktivní</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="97"/>
        <source>Today</source>
        <translation>Dnes</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="100"/>
        <source>Tomorrow</source>
        <translation>Zítra</translation>
    </message>
    <message>
        <location filename="../assets/tabs/TvTipy.qml" line="103"/>
        <source>Day after tomorrow</source>
        <translation>Týden</translation>
    </message>
</context>
<context>
    <name>VidelSomAction</name>
    <message>
        <source>I saw</source>
        <translation type="obsolete">Viděl jsem</translation>
    </message>
</context>
<context>
    <name>Vysielanie</name>
    <message>
        <location filename="../assets/pages/Vysielanie.qml" line="54"/>
        <source>Scroll to current</source>
        <translation>Aktuální</translation>
    </message>
    <message>
        <location filename="../assets/pages/Vysielanie.qml" line="107"/>
        <source>Today</source>
        <translation>Dnes</translation>
    </message>
    <message>
        <location filename="../assets/pages/Vysielanie.qml" line="111"/>
        <source>Tomorrow</source>
        <translation>Zítra</translation>
    </message>
    <message>
        <location filename="../assets/pages/Vysielanie.qml" line="115"/>
        <source>Day after tomorrow</source>
        <translation>Pozítří</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="30"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="44"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="57"/>
        <source>Rate</source>
        <translation>Hodnotit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="67"/>
        <source>Tell a Friend</source>
        <translation>Doporučit</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="94"/>
        <source>My FDB</source>
        <translation>Moje FDB.cz</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="95"/>
        <source>FDB account</source>
        <translation>Můj účet FDB.cz</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="106"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="107"/>
        <source>Find movie or personality</source>
        <translation>Hledat film nebo osobnost</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="118"/>
        <source>Now in tv</source>
        <translation>Právě běží</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="119"/>
        <source>Currently broadcast on tv</source>
        <translation>Aktuální běží v televizích</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="131"/>
        <source>Tv tips</source>
        <translation>Tv tipy</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="132"/>
        <source>What you should not miss</source>
        <translation>Co byste neměli zmeškat</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="143"/>
        <source>Favorite cinemas</source>
        <translation>Oblíbené kina</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="144"/>
        <source>Your favorite cinemas</source>
        <translation>Vaše oblíbené kina</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="155"/>
        <source>Play in cinemas</source>
        <translation>Hrají v kinech</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="156"/>
        <source>Playing in your favorite cinemas</source>
        <translation>Hrají v mých oblíbených kinech</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="167"/>
        <source>Cinema premieres</source>
        <translation>Kino premiéry</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="168"/>
        <source>Premieres, or what is currently running in theaters</source>
        <translation>Premiéry, nebo co právě běží v kinech</translation>
    </message>
</context>
</TS>
